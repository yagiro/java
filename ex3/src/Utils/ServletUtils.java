package Utils;

import Responses.BoardResponse;
import Responses.LobbyResponse;
//import logic.ServerLogic.GamesListManager;
import com.sun.javafx.geom.Vec2d;
import logic.ServerLogic.*;
import logic.ServerLogic.Exceptions.FileMissingException;
import logic.SupGame.Logic.CellInfo;
import logic.SupGame.Logic.CellStatus;
import logic.SupGame.Logic.MoveInfo;
import logic.SupGame.Logic.SupBoard;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Hagai on 10/6/16.
 *
 */

public class ServletUtils {
    public static <T> T getAttr(ServletContext i_ServletContext, String i_AttrName) {
        if (i_ServletContext == null) {
            return null;
        }

        T attrVal = (T) i_ServletContext.getAttribute(i_AttrName);
        return attrVal;
    }

    public static SupWebManager getWebManager(HttpServletRequest req) {
        return getWebManager(req.getServletContext());
    }

        public static SupWebManager getWebManager(ServletContext i_ServletContext) {
        if (i_ServletContext == null) {
            return null;
        }

        SupWebManager webManager = ServletUtils.getAttr(i_ServletContext, Constants.WebManager);
        if (webManager == null) {
            webManager = SupWebManager.getInstance();
            i_ServletContext.setAttribute(Constants.WebManager, webManager);
        }

        return webManager;
    }

    public static UsersManager getUsersManager(HttpServletRequest req) {
        return getWebManager(req.getServletContext()).getUsersManager();
    }

    public static UsersManager getUsersManager(ServletContext i_ServletContext) {
         return getWebManager(i_ServletContext).getUsersManager();
    }

    public static RoomsManager getRoomsManager(HttpServletRequest req) {
        return getWebManager(req.getServletContext()).getRoomsManager();
    }

    public static RoomsManager getRoomsManager(ServletContext i_ServletContext) {
        return getWebManager(i_ServletContext).getRoomsManager();
    }

    public static LobbyResponse getLobbyResponse(HttpServletRequest req, String i_UserName) {
        ServletContext servletContext = req.getServletContext();

        if (servletContext == null) {
            return null;
        }

        LobbyResponse lobbyResponse = new LobbyResponse(
                getUsersManager(servletContext).getUsers(),
                getRoomsManager(req).getRoomsInfo(),
                i_UserName);

        return lobbyResponse;
    }

    public static BoardResponse getBoardResponse(HttpServletRequest req, String i_RoomName){
        ServletContext servletContext = req.getServletContext();

        if(servletContext == null){
            return null;
        }

        RoomsManager roomsManager = getWebManager(servletContext).getRoomsManager();
        Room room = roomsManager.getRoom(i_RoomName);
        RoomInfo roomInfo = room.getInfo();
        BoardInfo boardInfo = roomsManager.getRoom(i_RoomName).getInfo().getBoardInfo();
        Vec2d boardSize = roomInfo.getBoardSize();
        BoardResponse boardResponse = new BoardResponse(boardInfo, boardSize);
        return boardResponse;
    }

//    public static File SaveFileFromRequest(HttpServletRequest req) throws Exception  {
//        String filePath;
//
//        filePath = "/Users/yakir/dev/projects/uploads/";
//
//        DiskFileItemFactory factory = new DiskFileItemFactory();
//        ServletFileUpload upload = new ServletFileUpload(factory);
//
//        List<FileItem> fileItems = upload.parseRequest(req);
//        FileItem fileToUpload = fileItems.get(0);
//        String fileName = fileToUpload.getName();
//        if (fileName.length() == 0) {
//            throw new FileMissingException();
//        }
//        File file = new File((filePath + (new Date()).getTime() + "_" + fileName));
//        fileToUpload.write(file);
//
//        return file;
//    }

    public static InputStream GetInputStreamFromFileInTheRequest(HttpServletRequest req) throws Exception  {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        List<FileItem> fileItems = upload.parseRequest(req);
        FileItem file = fileItems.get(0);
        InputStream inputStream = file.getInputStream();

        String fileName = file.getName();
        if (fileName.length() == 0) {
            throw new FileMissingException();
        }

        return inputStream;
    }

    public static MoveInfo ExtractMoveInfoFromRequest(Map<String,String[]> i_RequestToParse) {
        MoveInfo moveInfo;
        ArrayList<CellInfo> cellInfos = new ArrayList<>();
        String move = i_RequestToParse.get("moveType")[0];
        CellStatus moveType = CellStatus.Parse(move);
        int size = i_RequestToParse.size() - 1;
        size /= 2;
        CellInfo currCell;

        for (int i = 0; i < size; i++) {
            String xIdentifier = "selectedCells[" + i + "][x]";
            String yIdentifier = "selectedCells[" + i + "][y]";
            int x = Integer.parseInt(i_RequestToParse.get(xIdentifier)[0]);
            int y = Integer.parseInt(i_RequestToParse.get(yIdentifier)[0]);
            currCell = new CellInfo(x,y,moveType);
            cellInfos.add(currCell);
        }
        moveInfo = new MoveInfo(cellInfos);
        return moveInfo;
    }
}


