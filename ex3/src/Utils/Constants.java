package Utils;

/**
 * Created by yakir on 10/6/16.
 *
 */

public class Constants {
    public static final String Username = "username";
    public static final String PlayerType = "playerType";
    public static final String WebManager = "webManager";
    public static final String RoomName = "roomName";
    public static final String Name = "name";
    public static final String Type = "type";
}
