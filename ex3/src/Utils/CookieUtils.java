//package Utils;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * Created by yakir on 10/27/16.
// *
// */
//
//public class CookieUtils {
//    public static void AddCookie(HttpServletResponse res, String i_Name, String i_Value) {
//        res.addCookie(new Cookie(i_Name, i_Value));
//    }
//
//    public static void RemoveCookie(HttpServletResponse res, String i_Name) {
//        Cookie toRemove = new Cookie(i_Name, "");
//        toRemove.setMaxAge(0);
//        res.addCookie(toRemove);
//    }
//
////    public static void RemoveCookie(HttpServletRequest req, String i_Name) {
////        Cookie toRemove = getCookie(req, i_Name);
////        toRemove.setMaxAge(0);
////    }
//
//    public static Cookie getCookie(HttpServletRequest req, String i_Name) {
//        Cookie res = null;
//
//        for(Cookie cookie : req.getCookies()) {
//            if (cookie.getName().equals(i_Name)) {
//                res = cookie;
//                break;
//            }
//        }
//
//        return res;
//    }
//
//    public static void RegisterLogin(HttpServletResponse res, String i_Username) {
//        AddCookie(res, Constants.Username, i_Username);
//    }
//
//    public static void RegisterLogout(HttpServletResponse res) {
//        RemoveCookie(res, Constants.Username);
//    }
//
////    public static void RegisterLogout(HttpServletRequest req) {
////        RemoveCookie(req, Constants.Username);
////    }
//}
