package Utils;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by yakir on 10/13/16.
 *
 */

public class JsonUtils {
    private static final Gson m_Gson = new Gson();

    public static String toJson(Object i_ToJsonify) {
        return m_Gson.toJson(i_ToJsonify);
    }

    public static void sendJson(HttpServletResponse res, Object data) throws IOException {
        res.setContentType("application/json");
        res.setHeader("Access-Control-Allow-Origin", "*");
        try(PrintWriter out = res.getWriter()) {
            out.print(toJson(data));
            out.flush();
        }
    }

    public static void sendJson(HttpServletResponse res, int status, Object data) throws IOException {
        res.setStatus(status);
        JsonUtils.sendJson(res, data);
    }

    public static void sendJson(HttpServletResponse res, int status) throws IOException {
        JsonUtils.sendJson(res, status, "");
    }
}
