package Utils;

import logic.ServerLogic.Models.UserModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by yakir on 10/6/16.
 *
 */

public class SessionUtils {
    public static <T> T getAttr(HttpServletRequest req, String i_AttrName) {
        return SessionUtils.getAttr(req.getSession(), i_AttrName);
    }

    public static <T> T getAttr(HttpSession i_Session, String i_AttrName) {
        if (i_Session == null) {
            return null;
        }

        T attrVal = (T)i_Session.getAttribute(i_AttrName);

        return attrVal;
    }

    public static void RegisterLogout(HttpSession i_Session) {
        i_Session.removeAttribute(Constants.Username);
    }

    public static void RegisterJoin(HttpServletRequest req, String i_RoomName) {
        req.getSession().setAttribute(Constants.RoomName, i_RoomName);
    }

    public static void RegisterLogin(HttpServletRequest req, UserModel i_UserModel) {
        req.getSession().setAttribute(Constants.Username, i_UserModel.getUsername());
        req.getSession().setAttribute(Constants.PlayerType, i_UserModel.getPlayerType().name());
    }

    public static void RegisterLeave(HttpServletRequest req) {
        req.getSession().removeAttribute(Constants.RoomName);
    }

    public static void RegisterLeave(HttpSession i_Session) {
        i_Session.removeAttribute(Constants.RoomName);
    }

    public static boolean isEmptySession(HttpServletRequest req) {
        return getAttr(req, Constants.Username) == null;
    }
}
