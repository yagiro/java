package logic.Infrastructure;

import javafx.scene.Parent;

/**
 * Created by yakir on 9/6/16.
 *
 */

public class Mvc {

    public Mvc() {}

    private Object m_Ctrl;
    private Parent m_View;
    private Object m_Model;

    public Object getCtrl() {
        return m_Ctrl;
    }

    public void setCtrl(Object i_Ctrl) {
        this.m_Ctrl = i_Ctrl;
    }

    public Parent getView() {
        return m_View;
    }

    public void setView(Parent i_View) {
        this.m_View = i_View;
    }

    public Object getModel() {
        return m_Model;
    }

    public void setModel(Object i_Model) {
        this.m_Model = i_Model;
    }

}
