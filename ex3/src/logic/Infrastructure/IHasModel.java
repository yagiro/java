package logic.Infrastructure;

/**
 * Created by yakir on 9/6/16.
 */
public interface IHasModel {
    Object getModel();
}
