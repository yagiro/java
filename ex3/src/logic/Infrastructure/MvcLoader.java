package logic.Infrastructure;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.URL;

/**
 * Created by yakir on 9/6/16.
 *
 */

public class MvcLoader {

    public static Mvc LoadFromFxml(String i_FxmlPath) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader();
        URL url = (new Object()).getClass().getResource(i_FxmlPath);
        fxmlLoader.setLocation(url);
        Parent view = fxmlLoader.load(url.openStream());
        Object ctrl = fxmlLoader.getController();

        Mvc mvc = new Mvc();
        mvc.setCtrl(ctrl);
        mvc.setView(view);

        if (ctrl instanceof IHasModel) {
            mvc.setModel(((IHasModel) ctrl).getModel());
        }

        return mvc;
    }
}
