package logic.SupGame.JavaFxSup.Components.Board;

import logic.GameInfrastructure.PlayerType;
import logic.Infrastructure.IHasModel;
import logic.SupGame.JavaFxSup.Main.Controller.JavaFxSupCtrl;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupArcade;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;
import logic.SupGame.Logic.*;
import com.sun.javafx.geom.Vec2d;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Hagai on 9/8/16.
 *
 */

public class BoardCtrl implements IHasModel {

    private final Vec2d k_PrefBoardButtonSize = new Vec2d(25,25);
    private final Vec2d k_MinBoardButtonSize = new Vec2d(25,25);
    private ArrayList<JavaFxSupGame> m_Model;
    private JavaFxSupArcade m_Arcade;
    private ArrayList<CellInfo> m_SelectedCells = new ArrayList<>();
    private Tab m_CurrentTab;
    private ArrayList<BoardButton[][]> m_Boards;

    private JavaFxSupCtrl m_MainCtrl;

    @FXML private BorderPane ui_BoardBorderPane;
    @FXML private TabPane ui_GamesTabPane;

    private ToggleGroup m_MoveType;
    @FXML private RadioButton ui_FilledRadio;
    @FXML private RadioButton ui_BlankRadio;
    @FXML private RadioButton ui_UndefinedRadio;

    @FXML private Button ui_MakeMoveButton;
    @FXML private Button ui_UndoButton;
    @FXML private Button ui_RedoButton;
    @FXML private Button ui_NextTurnButton;
    @FXML private Pane ui_MakeMovePane;
    @FXML private VBox ui_GameControls;
    @FXML private Button ui_MovesHistoryButton;

    private HashMap<CellStatus, String> m_CellStatusToStyleClassMap;

    public BoardCtrl() {
        m_CellStatusToStyleClassMap = new HashMap<>();
        m_CellStatusToStyleClassMap.put(CellStatus.Filled, "button-filled");
        m_CellStatusToStyleClassMap.put(CellStatus.Blank, "button-blank");
        m_CellStatusToStyleClassMap.put(CellStatus.Undefined, "button");
    }

    public void Populate(JavaFxSupCtrl i_MainCtrl, ArrayList<JavaFxSupGame> i_Games, JavaFxSupArcade i_Arcade, JavaFxSupGame i_CurrentGame){
        m_MainCtrl = i_MainCtrl;
        m_Model = i_Games;
        m_Arcade = i_Arcade;
        createBoardsButtons(i_Arcade);
        populateGameTabPane(i_Games);
        ui_BoardBorderPane.setCenter(ui_GamesTabPane);
        ui_BoardBorderPane.getCenter().minHeight(600);
        ui_BoardBorderPane.getCenter().autosize();
        ui_BoardBorderPane.getCenter().prefHeight(600);
        ui_GamesTabPane.setMinWidth(400);
        ui_GamesTabPane.getTabs().get(0).getContent().minHeight(500);
        ui_GamesTabPane.getTabs().get(0).getContent().prefHeight(500);
        ui_GamesTabPane.getTabs().get(0).getContent().maxHeight(500);
        defineMoveTypeRadiosAsToggleGroup();
        m_CurrentTab = ui_GamesTabPane.getTabs().get(m_Arcade.getCurrentGameIndex());
        ui_MakeMovePane.setDisable(true);
        bindModelToUi();
        m_MainCtrl.isGameActiveProperty().addListener((isGameActiveProperty, oldValue, isGameActive) -> {
            if (isGameActive) {
                onGameSetToActive();
            }
        });

    }

    private void onGameSetToActive() {
        if(m_Arcade.getCurrentGame().getPlayer().getType() == PlayerType.Computer){
            doComputerRound();
            handleNextTurn();
        }
    }

    private void doComputerRound(){
        boolean computerChoseToSkip = false;
        System.out.println(m_Arcade.getCurrentGame().getPlayer().getName());
        while(m_Arcade.TurnsLeftProperty().getValue() > 0 && !computerChoseToSkip){
            computerChoseToSkip = DoComputerMove();
            if(!computerChoseToSkip) {
                onMoveMade();
            }
        }
        System.out.println("=============================================");
    }

    public boolean DoComputerMove(){
        boolean choseSkip = false;
        MoveType moveType = MoveType.getRandom();
        switch (moveType) {
            case Normal:
                m_Arcade.HandleComputerMove();
                break;
            case Undo:
                m_Arcade.UndoMove();
                System.out.println("Undo");
                break;
            case Redo:
                m_Arcade.RedoMove();
                System.out.println("Redo");
                break;
            case Skip:
                choseSkip = true;
                System.out.println("Skip");
                break;
            default:
                break;
        }
        return choseSkip;
    }

    private void bindModelToUi(){
        ui_GameControls.disableProperty().bind(m_MainCtrl.isGameActiveProperty().not().or(m_CurrentTab.selectedProperty().not()));
        ui_MakeMoveButton.disableProperty().bind(m_Arcade.AreTurnsOverProperty().and(m_Arcade.isSinglePlayerProperty().not()));
        ui_UndoButton.disableProperty().bind(m_Arcade.getCurrentGame().getMovesLog().HasMovesToUndoProperty().not());
        ui_RedoButton.disableProperty().bind(m_Arcade.getCurrentGame().getMovesLog().HasMovesToRedoProperty().not());
        ui_MovesHistoryButton.disableProperty().bind(m_MainCtrl.isGameActiveProperty().not().and(m_MainCtrl.isGameOverProperty().not()));
        ui_NextTurnButton.disableProperty().bind(m_Arcade.isSinglePlayerProperty());
    }

    private void createBoardsButtons(JavaFxSupArcade i_Arcade) {
        m_Boards = new ArrayList<>();
        i_Arcade.getGames().forEach((game) -> {
            m_Boards.add(new BoardButton[i_Arcade.getCurrentGame().getBoard().getNumOfRows()][i_Arcade.getCurrentGame().getBoard().getNumOfCols()]);
        });
    }

    private void defineMoveTypeRadiosAsToggleGroup() {
        m_MoveType = new ToggleGroup();
        ui_FilledRadio.setToggleGroup(m_MoveType);
        ui_FilledRadio.setSelected(true);
        ui_BlankRadio.setToggleGroup(m_MoveType);
        ui_UndefinedRadio.setToggleGroup(m_MoveType);
    }

    private void populateGameTabPane(ArrayList<JavaFxSupGame> i_Games) {
        m_Model = i_Games;
        int boardIdx = 0;
        for (JavaFxSupGame game : m_Model) {
            ui_GamesTabPane.getTabs().add(buildGameTab(game, boardIdx++));
        }
    }

    private Tab buildGameTab(JavaFxSupGame i_Game, int i_BoardIdx) {
        Tab gameTab = new Tab(i_Game.getPlayer().getName());
        gameTab.selectedProperty().addListener((tab, oldSelected, i_IsTabSelected) -> {
            if (i_IsTabSelected && m_MainCtrl.isGameOverProperty().getValue()) {
                m_Arcade.setCurrentGame(m_Arcade.getGames().get(i_BoardIdx));
            }
        });

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setContent(buildBoardGridPane(i_Game.getBoard(), i_BoardIdx, i_Game));

        gameTab.setContent(scrollPane);
        gameTab.disableProperty().bind(i_Game.getPlayer().IsComputerProperty().not().and(i_Game.getPlayer().IsPlayingProperty().not().and(m_MainCtrl.isGameActiveProperty())));
        gameTab.closableProperty().setValue(false);


        return gameTab;
    }

    private GridPane buildBoardGridPane(SupBoard i_Board, int i_BoardIdx, JavaFxSupGame i_Game) {
        GridPane boardGridPane = new GridPane();
        boardGridPane.setAlignment(Pos.CENTER);
        boardGridPane.setPrefSize(i_Board.getNumOfCols() + 100, i_Board.getNumOfRows() + 100);
        BoardButton currentButton = null;
        Label rowStreakLabel;

        String colStreaksStr;
        Label colStreakLabel;

        for (int i = 1 ; i <= i_Board.getNumOfCols(); ++i) {

            SupBoard.SingleStreak[] colStreaks = i_Board.getColStreaks().getStreaks().get(i-1).getStreakLengths();
            colStreaksStr = "";
            for (SupBoard.SingleStreak block : colStreaks) {
                colStreaksStr += block.getValue() + "\n";
            }
            colStreakLabel = new Label(colStreaksStr);
            colStreakLabel.autosize();
            colStreakLabel.setMinHeight(colStreaksStr.length() * 15);
            boardGridPane.add(colStreakLabel, i, 0);
            boardGridPane.setHalignment(colStreakLabel, HPos.CENTER);
            colStreakLabel.setAlignment(Pos.BOTTOM_CENTER);
        }

        for(int i = 0; i < i_Board.getNumOfRows(); ++i){

            String rowStreaks = "";
            for (SupBoard.SingleStreak block : i_Board.getRowStreaks().getStreaks().get(i).getStreakLengths()) {
                rowStreaks += " " + block.getValue();
            }
            rowStreakLabel = new Label(rowStreaks);
            rowStreakLabel.setMinWidth(rowStreaks.length() * 17);
            rowStreakLabel.setAlignment(Pos.CENTER_RIGHT);
            boardGridPane.add(rowStreakLabel, 0, i + 1);
            boardGridPane.setHalignment(rowStreakLabel, HPos.CENTER);


            for (int j = 0; j < i_Board.getNumOfCols(); ++j){
                currentButton = new BoardButton(i,j);
                currentButton.disableProperty().bind(i_Game.getPlayer().IsPlayingProperty().not()
                        .or(m_MainCtrl.isGameOverProperty()
                                .or(m_Arcade.AreTurnsOverProperty().and(m_Arcade.isSinglePlayerProperty().not()))));
                boardGridPane.add(currentButton, j+1, i+1);
                m_Boards.get(i_BoardIdx)[i][j] = currentButton;
            }
        }

        return boardGridPane;
    }

    private void boardButton_Click(ActionEvent event){
        BoardButton button = ((BoardButton)event.getSource());
        button.setIsSelected(!button.getIsSelected());

        if(button.getIsSelected()) {
            m_SelectedCells.add(new CellInfo(button.getRow(), button.getCol(), button.getButtonStatus()));
            button.getStyleClass().add("button-selected");
            ui_MakeMovePane.setDisable(false);
        }
        else {
            removeFromSelectedCells(button.getRow(), button.getCol());
            button.getStyleClass().remove("button-selected");
            if(m_SelectedCells.size() == 0){
                ui_MakeMovePane.setDisable(true);

            }
        }
    }

    private void removeFromSelectedCells(int i_Row, int i_Col) {
        CellInfo toRemove = null;
        for(CellInfo cell : m_SelectedCells) {
            if (cell.getRow() == i_Row && cell.getCol() == i_Col) {
                toRemove = cell;
                break;
            }
        }
        m_SelectedCells.remove(toRemove);
    }

    @FXML private void makeMoveButton_Click(ActionEvent actionEvent) {
        setSelectedCellsStatus();
        m_Arcade.DoMove(new MoveInfo(m_SelectedCells));
        onMoveMade();
    }

    private void onMoveMade(){
        updateBoard(m_Arcade.getCurrentGame().getBoard());
        m_SelectedCells = new ArrayList<>();
        ui_MakeMovePane.setDisable(true);
        checkIfGameIsOverAndHandle();
    }

    private void checkIfGameIsOverAndHandle() {
        if (m_Arcade.getCurrentGame().scoreProperty().getValue() == 100) {
            onPlayerWon();
        }
        else if (isNoMoreRounds()) {
            onNoMoreRounds();
        }
    }

    private void onNoMoreRounds() {
        m_MainCtrl.OnNoMoreRounds();
    }

    private void onPlayerWon() {
        m_MainCtrl.OnPlayerWon();
    }

    private boolean isNoMoreRounds() {
        return m_Arcade.roundsLeftProperty().getValue() == 0 && !m_Arcade.isSinglePlayerProperty().getValue();
    }

    private void setSelectedCellsStatus() {
        CellStatus chosenCellStatus = CellStatus.Parse(((RadioButton)m_MoveType.getSelectedToggle()).getText());
        m_SelectedCells.forEach((cellInfo -> cellInfo.setNewCellStatus(chosenCellStatus)));
    }

    private void handleNextTurn(){
        setNextGame();
        while(m_Arcade.getCurrentGame().getPlayer().getType() == PlayerType.Computer && !m_MainCtrl.isGameOverProperty().getValue()){
            doComputerRound();
            setNextGame();
            checkIfGameIsOverAndHandle();
        }

        if (!m_MainCtrl.isGameOverProperty().getValue()) {
            ui_FilledRadio.setSelected(true);
            ui_MakeMovePane.setDisable(true);
        }
        m_SelectedCells = new ArrayList<>();
    }

    private void setNextGame(){
        m_Arcade.MoveToNextGame();
        getNextTab();
        bindModelToUi();
    }

    @FXML private void nextTurnButton_Click(ActionEvent actionEvent) {
        handleNextTurn();
        onMoveMade();
    }

    @FXML private void undoMoveButton_Click(ActionEvent actionEvent) {
        m_Arcade.UndoMove();
        updateBoard(m_Arcade.getCurrentGame().getBoard());
        m_SelectedCells = new ArrayList<>();
        ui_MakeMovePane.setDisable(true);


    }

    @FXML private void redoMoveButton_Click(ActionEvent actionEvent) {
        m_Arcade.RedoMove();
        updateBoard(m_Arcade.getCurrentGame().getBoard());
        m_SelectedCells = new ArrayList<>();
        ui_MakeMovePane.setDisable(true);

    }

    @FXML private void showMovesHistory_Click(ActionEvent actionEvent) {
        showMovesHistory();
    }

    private void showMovesHistory() {
        m_MainCtrl.ShowMovesHistory();
    }

    @Override
    public Object getModel() {
        return m_Model;
    }

    private void getNextTab(){
        m_CurrentTab = ui_GamesTabPane.getTabs().get(m_Arcade.getCurrentGameIndex());
        ui_GamesTabPane.getSelectionModel().select(m_CurrentTab);
    }

    private void updateBoard(SupBoard i_Board){
        CellStatus currentStatus;
        BoardButton currentButton;
        for(int i = 0; i < i_Board.getNumOfRows(); ++i){
            for (int j = 0; j < i_Board.getNumOfCols(); ++j){
                currentStatus = i_Board.getCells()[i][j];
                currentButton = m_Boards.get(m_Arcade.getCurrentGameIndex())[i][j];
                currentButton.setButtonStatus(currentStatus);
                currentButton.getStyleClass().clear();
                currentButton.getStyleClass().add(m_CellStatusToStyleClassMap.get(currentStatus));
                currentButton.setIsSelected(false);
            }
        }
    }

    public void Reset() {
        m_Boards = new ArrayList<>();
        ui_GamesTabPane = new TabPane();
        m_SelectedCells = new ArrayList<>();
    }

    private class BoardButton extends Button{

        private int m_Row, m_Col;
        private CellStatus m_ButtonStatus;
        private boolean m_IsSelected;
        private SimpleStringProperty m_Status = new SimpleStringProperty();

        public BoardButton(int i_Row, int i_Col){

            this.setPrefSize(BoardCtrl.this.k_PrefBoardButtonSize.x, BoardCtrl.this.k_PrefBoardButtonSize.y);
            this.setMinSize(BoardCtrl.this.k_MinBoardButtonSize.x, BoardCtrl.this.k_MinBoardButtonSize.y);
            this.setOnAction((event) -> BoardCtrl.this.boardButton_Click(event));
            m_Row = i_Row;
            m_Col = i_Col;
            m_ButtonStatus = CellStatus.Undefined;
        }


        public int getRow() {
            return m_Row;
        }

        public int getCol() {
            return m_Col;
        }

        public boolean getIsSelected() {
            return m_IsSelected;
        }

        public void setIsSelected(boolean i_IsSelected) {
            this.m_IsSelected = i_IsSelected;
        }

        public CellStatus getButtonStatus() {
            return m_ButtonStatus;
        }

        public void setButtonStatus(CellStatus i_ButtonStatus) {
            this.m_ButtonStatus = i_ButtonStatus;
        }
    }
}

