package logic.SupGame.JavaFxSup.Components;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by yakir on 9/21/16.
 *
 */

public class PlayerRow {
    private SimpleIntegerProperty id;
    private SimpleStringProperty name;
    private SimpleIntegerProperty score;

    public Integer getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int i_Id) {
        this.id.set(i_Id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String i_Name) {
        this.name.set(i_Name);
    }

    public Integer getScore() {
        return score.get();
    }

    public SimpleIntegerProperty scoreProperty() {
        return score;
    }

    public void setScore(int i_Score) {
        this.score.set(i_Score);
    }

    public PlayerRow() {
        id = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        score = new SimpleIntegerProperty();
    }

    public PlayerRow(Integer i_Id, String i_Name, SimpleIntegerProperty i_Score) {
        this();

        id.setValue(i_Id);
        name.setValue(i_Name);
        score.bind(i_Score);
    }
}
