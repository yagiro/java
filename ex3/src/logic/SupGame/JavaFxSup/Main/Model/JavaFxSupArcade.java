package logic.SupGame.JavaFxSup.Main.Model;

import logic.GameInfrastructure.PlayerType;
import logic.Generated.GameDescriptor;
import logic.Generated.Player;
import logic.Generated.Players;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Logic.GameType;
import logic.SupGame.Logic.MoveInfo;
import logic.SupGame.Utils.XmlUtils;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.File;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by yakir on 9/5/16.
 *
 */

public class JavaFxSupArcade extends Observable {

    protected final int k_DefaultTurnsLeft = 2;
    protected ArrayList<JavaFxSupGame> m_Games;
    protected SimpleIntegerProperty m_MaxRoundsProperty;
    protected SimpleIntegerProperty m_RoundsLeftProperty;
    protected JavaFxSupGame m_CurrentGame;
    protected SimpleIntegerProperty m_TurnsLeftProperty = new SimpleIntegerProperty();
    protected SimpleIntegerProperty m_CurrentGameIndexProperty = new SimpleIntegerProperty();
    protected SimpleBooleanProperty m_AreTurnsOver = new SimpleBooleanProperty();
    protected SimpleIntegerProperty m_PlayerIdProperty;
    protected SimpleStringProperty m_PlayerNameProperty;
    protected SimpleIntegerProperty m_ScoreProperty;
    protected SimpleIntegerProperty m_TurnNumberProperty;
    protected SimpleIntegerProperty m_RoundNumberProperty;
    protected SimpleBooleanProperty m_IsGameActiveProperty;
    protected SimpleBooleanProperty m_IsGameOverProperty;
    protected SimpleBooleanProperty m_IsGameFileLoadedProperty;
    protected SimpleBooleanProperty m_IsSinglePlayerProperty;


    public JavaFxSupArcade() {
        m_Games = new ArrayList<>();
        m_CurrentGameIndexProperty.setValue(0);
        m_TurnsLeftProperty.setValue(k_DefaultTurnsLeft);
        m_AreTurnsOver.setValue(false);
        m_RoundsLeftProperty = new SimpleIntegerProperty();
        m_PlayerIdProperty = new SimpleIntegerProperty();
        m_PlayerNameProperty = new SimpleStringProperty();
        m_ScoreProperty = new SimpleIntegerProperty();
        m_TurnsLeftProperty = new SimpleIntegerProperty();
        m_TurnNumberProperty = new SimpleIntegerProperty();
        m_RoundNumberProperty = new SimpleIntegerProperty();
        m_MaxRoundsProperty = new SimpleIntegerProperty();
        m_IsGameOverProperty = new SimpleBooleanProperty();
        m_IsGameActiveProperty = new SimpleBooleanProperty();
        m_IsGameFileLoadedProperty = new SimpleBooleanProperty();
        m_IsSinglePlayerProperty = new SimpleBooleanProperty();
    }

    public SimpleBooleanProperty isSinglePlayerProperty() { return m_IsSinglePlayerProperty; }

    public SimpleBooleanProperty isGameActiveProperty() {
        return m_IsGameActiveProperty;
    }

    public SimpleBooleanProperty isGameFileLoadedProperty() {
        return m_IsGameFileLoadedProperty;
    }

    public SimpleBooleanProperty isGameOverProperty() {
        return  m_IsGameOverProperty;
    }

    public int getMaxTurns() {
        return k_DefaultTurnsLeft;
    }

    public SimpleIntegerProperty maxRoundsProperty() {
        return m_MaxRoundsProperty;
    }

    public int getCurrentGameIndex() {
        return m_CurrentGameIndexProperty.get();
    }


    public void LoadGameFileAndInitialize(File i_GameFile) throws GameDescriptorException {
        GameDescriptor gameDescriptor = XmlUtils.ParseGameDescriptorFromXml(i_GameFile);
        checkIfSinglePlayerAndHandle(gameDescriptor);
        initializeMembers(gameDescriptor);
    }

    private void checkIfSinglePlayerAndHandle(GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        GameType gameType;

        try {
            gameType = GameType.Parse(i_GameDescriptor.getGameType());

            m_IsSinglePlayerProperty.setValue(false);

            if (gameType == GameType.SinglePlayer) {
                m_IsSinglePlayerProperty.setValue(true);
                wrapSinglePlayerWithMultiPlayer(i_GameDescriptor);
            }
        }
        catch(ParseException pe) {
            throw new GameDescriptorException("GameType is not valid");
        }
        catch(Exception e) {
            throw new GameDescriptorException("Missing GameType field");
        }

    }

    private void wrapSinglePlayerWithMultiPlayer(GameDescriptor i_GameDescriptor) {
        Player player = new Player();
        player.setId(BigInteger.ONE);
        player.setName("Jesus");
        player.setPlayerType(PlayerType.Human.toString());

        Players players = new Players();
        players.getPlayer().add(player);

        GameDescriptor.MultiPlayers multiPlayers = new GameDescriptor.MultiPlayers();
        multiPlayers.setPlayers(players);
        multiPlayers.setMoves("0");

        i_GameDescriptor.setMultiPlayers(multiPlayers);
    }

    private void initializeMembers(GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        initializeGames(i_GameDescriptor);
        m_TurnsLeftProperty.setValue(k_DefaultTurnsLeft);
        try {
            m_MaxRoundsProperty.setValue(Integer.parseInt(i_GameDescriptor.getMultiPlayers().getMoves()));
            m_RoundsLeftProperty.setValue(m_MaxRoundsProperty.getValue());
        }
        catch(Exception e) {
            throw new GameDescriptorException("Invalid attribute (moves OR roundLeft) in Multiplayers object");
        }m_AreTurnsOver.setValue(false);
    }

    private void initializeGames(GameDescriptor i_GameDescriptor) throws GameDescriptorException {

        m_CurrentGameIndexProperty.set(0);
        m_Games.clear();

        if(i_GameDescriptor != null) {
            List<Player> rawPlayers = i_GameDescriptor.getMultiPlayers().getPlayers().getPlayer();
            ArrayList<BigInteger> ids = new ArrayList<>();
            for (Player rawPlayer : rawPlayers) {
                if (ids.contains(rawPlayer.getId())) {
                    throw new GameDescriptorException("Each player must have a unique ID");
                }
                ids.add(rawPlayer.getId());
                m_Games.add(new JavaFxSupGame(rawPlayer, i_GameDescriptor));
            }
        }

        m_CurrentGame = m_Games.get(m_CurrentGameIndexProperty.getValue());
        m_CurrentGame.getPlayer().IsPlayingProperty().setValue(true);
    }

    private void updateProperties() {
        m_PlayerIdProperty.setValue(m_CurrentGame.getPlayer().getId());
        m_PlayerNameProperty.setValue(m_CurrentGame.getPlayer().getName());
        try {
            m_ScoreProperty.setValue(m_CurrentGame.scoreProperty().getValue());
        }
        catch (Exception e) {
            // in case we tried to use m_CurrentGame.scoreProperty() while m_StatisticsManager == null
        }
        m_TurnNumberProperty.setValue(k_DefaultTurnsLeft - m_TurnsLeftProperty.getValue() + 1); // because starts with zero
        m_RoundNumberProperty.setValue(m_MaxRoundsProperty.getValue() - m_RoundsLeftProperty.getValue() + 1); // because starts with zero
//        if (m_RoundNumberProperty.getValue() > m_MaxRoundsProperty.getValue()) {
//            m_RoundNumberProperty.setValue(m_RoundNumberProperty.getValue() - 1);
//        }
    }

    public ArrayList<JavaFxSupGame> getGames() {
        return m_Games;
    }

    public void DoMove(MoveInfo i_MoveInfo){
        m_CurrentGame.MakeMove(i_MoveInfo);
        decrementNumOfTurns();
        updateProperties();
    }

    public void HandleComputerMove(){
        m_CurrentGame.DoComputerMove();
        decrementNumOfTurns();
    }

    public void UndoMove(){
       m_CurrentGame.MakeUndoMove();
        incrementNumOfTurns();
        updateProperties();
    }

    public void RedoMove(){
        if(m_CurrentGame.getMovesLog().CanRedo()) {
            m_CurrentGame.MakeRedoMove();
            decrementNumOfTurns();
            updateProperties();
        }
    }
    public void MoveToNextGame(){
        setNextIteration();
        m_CurrentGame = m_Games.get(m_CurrentGameIndexProperty.getValue());
        m_CurrentGame.getPlayer().IsPlayingProperty().setValue(true);
        updateProperties();
    }

    private void incrementNumOfTurns(){
        if(m_TurnsLeftProperty.getValue() < k_DefaultTurnsLeft) {
            m_TurnsLeftProperty.setValue(m_TurnsLeftProperty.getValue() + 1);
        }
        m_AreTurnsOver.setValue(false);
    }

    private  void decrementNumOfTurns(){
        m_TurnsLeftProperty.setValue(m_TurnsLeftProperty.getValue() - 1);
        if(m_TurnsLeftProperty.getValue() == 0){
            m_AreTurnsOver.setValue(true);
        }
    }

    private void setNextIteration(){
        if(m_Games.contains(m_CurrentGame)) {
            m_CurrentGame.getPlayer().IsPlayingProperty().setValue(false);
        }
        IncrementGameNumber();
        m_TurnsLeftProperty.setValue(k_DefaultTurnsLeft);
        m_AreTurnsOver.setValue(false);
    }

    public JavaFxSupGame getCurrentGame() {
        return m_CurrentGame;
    }

    public void setCurrentGame(JavaFxSupGame i_Game) {
        if (isGameOverProperty().getValue()) {
            m_CurrentGame = i_Game;
            updateProperties();
        }
    }

    private void IncrementGameNumber(){
        int NumOfGames = m_Games.size();
        boolean negativeIdx = m_CurrentGameIndexProperty.getValue() < 0;
        m_CurrentGameIndexProperty.setValue((m_CurrentGameIndexProperty.getValue() + 1) % NumOfGames);
        if(m_CurrentGameIndexProperty.getValue() == 0 && !negativeIdx){
            m_RoundsLeftProperty.setValue(m_RoundsLeftProperty.getValue() - 1);
        }
    }

    public void Reload() {
        m_Games.forEach((supGame) -> supGame.Start());
        updateProperties();
    }

    public SimpleIntegerProperty roundNumberProperty() { return m_RoundNumberProperty; }

    public SimpleIntegerProperty roundsLeftProperty() {
        return m_RoundsLeftProperty;
    }

    public SimpleIntegerProperty TurnsLeftProperty() {
        return m_TurnsLeftProperty;
    }

    public SimpleBooleanProperty AreTurnsOverProperty() {
        return m_AreTurnsOver;
    }

    public SimpleIntegerProperty playerIdProperty() {
        return m_PlayerIdProperty;
    }

    public SimpleStringProperty playerNameProperty() {
        return m_PlayerNameProperty;
    }

    public SimpleIntegerProperty scoreProperty() {
        return m_ScoreProperty;
    }

    public SimpleIntegerProperty turnNumberProperty() {
        return m_TurnNumberProperty;
    }
}
