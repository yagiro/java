package logic.SupGame.JavaFxSup.Main.Model;

import logic.Generated.GameDescriptor;
import logic.ServerLogic.Models.UserModel;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Logic.MoveInfo;
import logic.SupGame.Logic.Player;
import logic.SupGame.Logic.SupBoard;
import logic.SupGame.SupGame;

/**
 * Created by yakir on 9/5/16.
 *
 */

public class JavaFxSupGame extends SupGame {
    private Player m_Player;

    public JavaFxSupGame(logic.Generated.Player i_RawPlayer, GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        m_Player = new Player(i_RawPlayer);
        populateMembers(i_GameDescriptor);
        m_IsGameDescriptorLoaded = true;

    }

    public JavaFxSupGame(UserModel i_UserModel, GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        m_Player = new Player(i_UserModel);
        populateMembers(i_GameDescriptor);
        m_IsGameDescriptorLoaded = true;
    }

    public void MakeMove(MoveInfo i_MoveInfo){
        super.DoMove(i_MoveInfo);
    }

    public void MakeUndoMove(){
        super.UndoMove();
    }

    public void MakeRedoMove() {
        super.RedoMove();
    }

    @Override
    public void Run() {

    }

    @Override
    protected void HandleWin() {

    }

    public void Start() {
        super.OnStartGame(m_Player.getType());
    }

    public Player getPlayer() {
        return m_Player;
    }

    public SupBoard getBoard() {
        return m_Board;
    }
}
