package logic.SupGame.JavaFxSup.Main.Controller;

import logic.Infrastructure.Mvc;
import logic.Infrastructure.MvcLoader;
import logic.SupGame.JavaFxSup.Components.Board.BoardCtrl;
import logic.SupGame.JavaFxSup.Components.PlayerRow;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupArcade;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Logic.MoveInfo;
import com.sun.istack.internal.Nullable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/**
 * Created by yakir on 9/5/16.
 *
 */


public class JavaFxSupCtrl {

    private JavaFxSupArcade m_Model;
    private Stage m_PrimaryStage;
    @FXML private BorderPane ui_BorderPanel;
    private Mvc m_BoardMvc;
    private Stage m_HistoryStage;

    private File m_LoadedGameFile;
    @FXML private Button ui_EndGameButton;
    @FXML private VBox ui_GameInfo;
    @FXML private Button ui_StartGameButton;
    @FXML private Button ui_LoadGameButton;

    @FXML private Label ui_RoundLabel;
    @FXML private Label ui_PlayerIdLabel;
    @FXML private Label ui_PlayerNameLabel;
    @FXML private Label ui_ScoreLabel;
    @FXML private Label ui_TurnNumberLabel;
    private TableView<PlayerRow> ui_PlayersTable;

    public JavaFxSupCtrl() {
        m_Model = new JavaFxSupArcade();
        m_HistoryStage = new Stage();
        m_HistoryStage.setMaxWidth(300);
    }

    private void bindModelToUi() {
        ui_EndGameButton.disableProperty().bind(m_Model.isGameActiveProperty().not());
        m_BoardMvc.getView().disableProperty().bind(m_Model.isGameActiveProperty().not().and(isGameOverProperty().not()));
        ui_GameInfo.disableProperty().bind(m_Model.isGameActiveProperty().not());
        ui_GameInfo.visibleProperty().bind(m_Model.isGameActiveProperty().or(m_Model.isGameOverProperty()));
        ui_StartGameButton.disableProperty().bind(m_Model.isGameActiveProperty().or(m_Model.isGameFileLoadedProperty().not()));
        ui_LoadGameButton.disableProperty().bind(m_Model.isGameActiveProperty());

        bindUiToModelAccordingToGameType();
    }

    private void bindUiToModelAccordingToGameType() {
        ui_PlayerIdLabel.textProperty().bind(Bindings.concat("ID: ", m_Model.playerIdProperty()));
        ui_PlayerNameLabel.textProperty().bind(Bindings.concat("Name: ", m_Model.playerNameProperty()));
        ui_ScoreLabel.textProperty().bind(Bindings.concat("Score: ", m_Model.scoreProperty()));

        if (m_Model.isSinglePlayerProperty().getValue()) {
            ui_RoundLabel.textProperty().unbind();
            ui_RoundLabel.textProperty().setValue("");
            ui_TurnNumberLabel.textProperty().bind(Bindings.concat("Move: ", m_Model.turnNumberProperty()));
        }
        else {
            ui_RoundLabel.textProperty().bind(Bindings.concat("Round: ", m_Model.roundNumberProperty()," / ", m_Model.maxRoundsProperty()));
            ui_TurnNumberLabel.textProperty().bind(Bindings.concat("Move: ", m_Model.turnNumberProperty(), " / ", m_Model.getMaxTurns()));
        }
    }

    public void InitializeComponents(BorderPane i_MainView, Stage i_PrimaryStage) {
        m_PrimaryStage = i_PrimaryStage;

        try {
            m_BoardMvc = MvcLoader.LoadFromFxml("/logic/SupGame/JavaFxSup/Components/Board/BoardView.fxml");
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        bindModelToUi();
        initializeStage();
        stylizeComponents();
    }

    private void stylizeComponents() {
        ui_StartGameButton.setMinWidth(ui_LoadGameButton.getPrefWidth());
        ui_StartGameButton.setPrefWidth(ui_LoadGameButton.getPrefWidth());
        ui_EndGameButton.setPrefWidth(ui_LoadGameButton.getPrefWidth());
    }


    private void initializeStage() {
        ui_BorderPanel.setCenter(m_BoardMvc.getView());
    }

    @FXML
    private void btnLoadGameFile_Click(ActionEvent actionEvent) {
        m_LoadedGameFile = openFileChooser();
        m_Model.isGameOverProperty().setValue(false);
        loadGameFileAndInitialize();
    }

    private void loadGameFileAndInitialize() {
        if (m_LoadedGameFile != null) {
            try {
                m_Model.LoadGameFileAndInitialize(m_LoadedGameFile);
                ((BoardCtrl)m_BoardMvc.getCtrl()).Reset();
                ((BoardCtrl)m_BoardMvc.getCtrl()).Populate(this, m_Model.getGames(), m_Model, m_Model.getCurrentGame());
                m_Model.isGameFileLoadedProperty().setValue(true);
                m_Model.Reload();
                initAndBindPlayersTableToModel();
            }
            catch (GameDescriptorException gde) {
                ShowAlert(AlertType.ERROR, "Invalid Game File: " + gde.getMessage());
            }
            catch (Exception e) {
                e.printStackTrace();
                ShowAlert(AlertType.ERROR, e.getMessage());
            }
        }
    }

    private void initAndBindPlayersTableToModel() {
        if (ui_PlayersTable == null) {
            ui_PlayersTable = new TableView<PlayerRow>();
            setPlayersTableCols();

            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            scrollPane.setContent(ui_PlayersTable);

            ui_GameInfo.getChildren().add(scrollPane);
        }

        ObservableList<PlayerRow> players = getPlayerRows();
        ui_PlayersTable.setItems(players);
    }

    private void setPlayersTableCols() {
        TableColumn idCol = new TableColumn("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<PlayerRow, Integer>("id"));

        TableColumn nameCol = new TableColumn("Name");
        nameCol.setPrefWidth(100);
        nameCol.setCellValueFactory(new PropertyValueFactory<PlayerRow, String>("name"));

        TableColumn scoreCol = new TableColumn("Score");
        scoreCol.setPrefWidth(100);

        scoreCol.setCellValueFactory(new PropertyValueFactory<PlayerRow, Integer>("score"));

        ui_PlayersTable.getColumns().addAll(idCol, nameCol, scoreCol);
    }

    private ObservableList<PlayerRow> getPlayerRows() {
        ObservableList<PlayerRow> playerRows = FXCollections.observableArrayList();

        for (JavaFxSupGame game : m_Model.getGames()) {
            Integer id = game.getPlayer().getId().intValue();
            String name = game.getPlayer().getName();
            SimpleIntegerProperty scoreProperty = game.scoreProperty();

            playerRows.add(new PlayerRow(id, name, scoreProperty));
        }

        return playerRows;
    }

    @FXML
    private void startGameButton_Click(ActionEvent ae) {
        startMultiplayerGame();
    }

    private void startMultiplayerGame() {
        if (m_Model.isGameOverProperty().getValue()) {
            m_Model.isGameOverProperty().setValue(false);
            reset();
        }
        bindUiToModelAccordingToGameType();
        m_Model.isGameOverProperty().setValue(false);
        m_Model.isGameActiveProperty().setValue(true);
    }

    private void reset() {
        loadGameFileAndInitialize();
    }

    public SimpleBooleanProperty isGameActiveProperty()
    {
        return m_Model.isGameActiveProperty();
    }

    @FXML private void endGameButton_Click(ActionEvent ae) {
        endGame();
    }

    @Nullable
    private File openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select XML Game File");
        fileChooser.setInitialDirectory(new File("/"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("xml files", "*.xml"));
        return fileChooser.showOpenDialog(m_PrimaryStage);
    }

    public void ShowAlert(AlertType i_AlertType, String i_Message) {
        (new Alert(i_AlertType, i_Message)).show();
    }

    public void OnNoMoreRounds() {
        endGame();
    }

    private void endGame() {
        m_Model.isGameActiveProperty().setValue(false);
        m_Model.isGameOverProperty().setValue(true);
    }

    public void OnPlayerWon() {
        m_Model.isGameActiveProperty().setValue(false);
        m_Model.isGameOverProperty().setValue(true);
        String victoryMessage = "Congratulations! " + m_Model.getCurrentGame().getPlayer().getName() + " wins the game! :)";
        ShowAlert(AlertType.INFORMATION, victoryMessage);
    }

    public SimpleBooleanProperty isGameOverProperty() {
        return m_Model.isGameOverProperty();
    }

//    public SimpleBooleanProperty isSinglePlayerProperty() {
//        return m_Model.isSinglePlayerProperty();
//    }

    public void ShowMovesHistory() {
        Scene scene = new Scene(buildHistoryView());
        m_HistoryStage.setScene(scene);
        m_HistoryStage.show();
    }

    private Parent buildHistoryView() {
        ListView movesList = new ListView();

        for(MoveInfo move : m_Model.getCurrentGame().getMovesLog().getStack()) {
            movesList.getItems().add(move.toString());
        }

        return movesList;
    }
}
