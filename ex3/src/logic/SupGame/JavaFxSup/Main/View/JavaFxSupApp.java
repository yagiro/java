package logic.SupGame.JavaFxSup.Main.View;

import logic.Infrastructure.Mvc;
import logic.Infrastructure.MvcLoader;
import logic.SupGame.JavaFxSup.Main.Controller.JavaFxSupCtrl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Created by Hagai on 9/5/16.
 *
 */

public class JavaFxSupApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Mvc mainMvc = MvcLoader.LoadFromFxml("/logic/SupGame/JavaFxSup/Main/View/JavaFxSupView.fxml");
        BorderPane mainView = (BorderPane) mainMvc.getView();
        JavaFxSupCtrl mainCtrl = (JavaFxSupCtrl) mainMvc.getCtrl();

        mainCtrl.InitializeComponents(mainView, primaryStage);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(mainView);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        primaryStage.setTitle("H&Y: Black & Solve");
        Scene scene = new Scene(scrollPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
