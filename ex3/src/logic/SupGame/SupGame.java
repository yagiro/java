package logic.SupGame;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import logic.GameInfrastructure.Game;
import logic.GameInfrastructure.PlayerType;
import logic.Generated.GameDescriptor;
import logic.SupGame.Logic.*;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Utils.XmlUtils;
import com.sun.istack.internal.Nullable;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by yakir on 8/1/16.
 *
 */

public abstract class SupGame extends Game {

    protected final int k_MaxNumOfComputerMoves = 20;
    protected Solution m_Solution;
    protected SupBoard m_Board;
    protected MovesLog m_MovesLog;
    protected StatisticsManager m_StatisticsManager;
    protected PlayerType m_PlayerType;
    protected boolean m_IsGameDescriptorLoaded;
    protected boolean m_IsActiveGameSession;
    protected boolean m_PlayerWon;

    public SupGame() {
        m_MovesLog = new MovesLog();
    }

    public SimpleIntegerProperty scoreProperty() {
        return m_StatisticsManager.scoreProperty();
    }

    protected void readGameFile(String i_GameFilePath) throws GameDescriptorException {
        GameDescriptor gameDescriptor = XmlUtils.ParseGameDescriptorFromXml(i_GameFilePath);
        populateMembers(gameDescriptor);
    }

    public Solution getSolution() {
        return m_Solution;
    }

    protected void populateMembers(GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        m_Board = new SupBoard(i_GameDescriptor.getBoard());
        m_Solution = new Solution(i_GameDescriptor.getBoard());

//        validateSumOfStreakVsNumOfSolutionFilledCells();
    }

    @Deprecated
    private void validateSumOfStreakVsNumOfSolutionFilledCells() throws GameDescriptorException {
        int numOfSolutionFilledCells = getNumOfSolutionFilledCells();
        if (getSumOfStreaks(m_Board.getRowStreaks()) != numOfSolutionFilledCells
                || getSumOfStreaks(m_Board.getColStreaks()) != numOfSolutionFilledCells) {
            throw new GameDescriptorException("Something doesn't add up. The amount of filled cells according to the solution, does not match the number of filled cells according to the slices definition.");
        }
    }

    private int getNumOfSolutionFilledCells() {
        return m_Solution.getCellCoords().size();
    }

    private int getSumOfStreaks(SupBoard.Streaks i_Streaks) {
        int sumOfFilledCells = 0;

        for(SupBoard.Streak streak : i_Streaks.getStreaks()){
            for(SupBoard.SingleStreak streakLength : streak.getStreakLengths()) {
                sumOfFilledCells += streakLength.getValue();
            }
        }

        return sumOfFilledCells;
    }

    protected void OnStartGame(PlayerType i_PlayerType) {
        Reset();
        m_IsActiveGameSession = true;
        m_StatisticsManager = new StatisticsManager(m_Board, m_Solution, new Date());
        m_PlayerType = i_PlayerType;
    }

    protected boolean DoMove(MoveInfo i_MoveInfo) {
        boolean isValidMove = m_Board.IsValidMove(i_MoveInfo);

        if (isValidMove) {
            m_Board.DoMove(i_MoveInfo);
            m_MovesLog.HandleAndLogMove(i_MoveInfo);
            m_StatisticsManager.incrementNumOfMoves();

            checkPerfectBlocks();
            checkVictory();
        }

        return isValidMove;
    }

    protected void checkPerfectBlocks() {
        clearPerfectStreaks();
        for(int i = 0; i < m_Board.getNumOfRows(); ++i) {
            ArrayList<SupBoard.Streak> rowStreaks = m_Board.getRowStreaks().getStreaks();
            SupBoard.Streak rowStreak = rowStreaks.get(i);
            markPerfectBlocks(rowStreak, m_Board.getCells()[i], Orientation.Row, i);
        }
        for(int i = 0; i < m_Board.getNumOfCols(); ++i) {
            SupBoard.Streak colStreak = m_Board.getColStreaks().getStreaks().get(i);
            CellStatus[] colCells = buildColCells(i);
            markPerfectBlocks(colStreak, colCells, Orientation.Column, i);
        }
    }

    private CellStatus[] buildColCells(int i_Idx) {
        CellStatus[] colCells = new CellStatus[m_Board.getNumOfRows()];

        for (int i = 0; i < m_Board.getNumOfRows(); ++i) {
            colCells[i] = m_Board.getCells()[i][i_Idx];
        }

        return colCells;
    }

    private void clearPerfectStreaks() {
        for(SupBoard.Streak streak : m_Board.getRowStreaks().getStreaks()) {
            for(SupBoard.SingleStreak singleStreak : streak.getStreakLengths()){
                singleStreak.setPerfect(false);
            }
        }
        for(SupBoard.Streak streak : m_Board.getColStreaks().getStreaks()) {
            for(SupBoard.SingleStreak singleStreak : streak.getStreakLengths()){
                singleStreak.setPerfect(false);
            }
        }
    }

    private void markPerfectBlocks(SupBoard.Streak i_Streak, CellStatus[] i_Cells, Orientation i_Orientation, int i_Id) {

        ArrayList<Block> blocks = buildBlocks(i_Cells);
        SupBoard.SingleStreak perfectStreak = new SupBoard.SingleStreak();
        SupBoard.SingleStreak[] streaks = i_Streak.getStreakLengths();
        HashMap<Integer,Integer> blockCounts =  getLengthCounts(blocks);
        HashMap<Integer,Integer> streakCounts = getLengthStreaks(i_Streak.getStreakLengths());

        HashMap<Integer, Boolean> lengthCheckedMap = new HashMap<>();

        int matchCounter = 0;
        int leftOfBlock, rightOfBlock, leftOfStreak, rightOfStreak;
        boolean wasPerfectFoundInScan;
        boolean allPerfect = false;
        do {
            wasPerfectFoundInScan = false;
            for (Block block : blocks) {
                allPerfect = false;
                if (!block.isPerfect) {
                    leftOfBlock = block.left;
                    rightOfBlock = i_Cells.length - block.right - 1;
                    if(blockCounts.get(block.length) == streakCounts.get(block.length) && !wasLengthChecked(block.length,lengthCheckedMap)){
                        allPerfect = checkIfAllPerfect(blocks, i_Streak.getStreakLengths(), block.length,
                                i_Cells.length,blockCounts.get(block.length));
                        lengthCheckedMap.put(block.length, true);
                    }
                    if(!allPerfect) {
                        matchCounter = 0;
                        for (int i = 0; i < streaks.length; i++) {
                            if(!streaks[i].isPerfect()) {
                                if (isMatch(block, leftOfBlock, rightOfBlock, streaks, i)) {
                                    matchCounter++;
                                    if (matchCounter == 1) {
                                        perfectStreak = streaks[i];
                                    }
                                }
                            }
                        }
                        //if a single match was found for the block after full scan of streaks, block is perfect
                        if (matchCounter == 1) {
                            wasPerfectFoundInScan = true;
                            block.isPerfect = true;
                            perfectStreak.setPerfect(true);
                        }
                    }
                    else{
                        wasPerfectFoundInScan = true;
                    }
                }
            }
        }while (wasPerfectFoundInScan);
    }


    private boolean isMatch(Block block, int leftOfBlock, int rightOfBlock, SupBoard.SingleStreak[] streaks, int i_Index){
            boolean isMatch = false;
        //find match for block
            if (isStreakOfSameValue(streaks[i_Index], block)) {
                if (leftOfBlock >= sumOfStreaksLeftOfStreak(streaks, i_Index) &&
                        rightOfBlock >= sumOfStreaksRightOfStreak(streaks, i_Index)) {
                        isMatch = true;
                    }
                }

            return isMatch;
    }

    private boolean wasLengthChecked(int i_length, HashMap<Integer,Boolean> i_Map){
        return i_Map.containsKey(i_length) && i_Map.get(i_length);
    }

    private boolean checkIfAllPerfect(ArrayList<Block> i_Blocks, SupBoard.SingleStreak[] i_StreakLengths,
                                      int i_Length, int i_CellsSize, int i_NumOfBlocks) {
        boolean allPerfect = false;
        int leftOfBlock;
        int rightOfBlock;
        int index = 0;
        int perfectCounter = 0;
        ArrayList<Block> blocksOfLength = buildBlocksOfLength(i_Blocks, i_Length);
        ArrayList<SupBoard.SingleStreak> streaksOfLength = buildStreaksOfLength(i_StreakLengths, i_Length);
        for(Block block : blocksOfLength){
            leftOfBlock = block.left;
            rightOfBlock = i_CellsSize - block.right - 1;
            for(int i = index; i < i_StreakLengths.length; i++){
                if(i_StreakLengths[i].getValue() == i_Length){
                    index = i + 1;
                    if(isMatch(block, leftOfBlock,rightOfBlock,i_StreakLengths, i)){
                        perfectCounter++;
                    }
                }
            }
        }
        if(perfectCounter == i_NumOfBlocks){
            allPerfect = true;
            markAllPerfect(i_Blocks,i_StreakLengths, i_Length);
        }
        return allPerfect;
    }

    private void markAllPerfect(ArrayList<Block> i_Blocks, SupBoard.SingleStreak[] i_StreakLengths, int i_Length) {
        for(Block block : i_Blocks){
            if(block.length == i_Length){
                block.isPerfect = true;
            }
        }

        for(SupBoard.SingleStreak streak : i_StreakLengths){
            if(streak.getValue() == i_Length){
                streak.setPerfect(true);
            }
        }
    }

    private ArrayList<SupBoard.SingleStreak> buildStreaksOfLength(SupBoard.SingleStreak[] i_StreakLengths, int i_Length) {
        ArrayList<SupBoard.SingleStreak> res = new ArrayList<>();
        for(SupBoard.SingleStreak streak: i_StreakLengths){
            if(streak.getValue() == i_Length){
                res.add(streak);
            }
        }
        return res;
    }

    private ArrayList<Block> buildBlocksOfLength(ArrayList<Block> i_Blocks, int i_Length) {
        ArrayList<Block> res = new ArrayList<>();
        for(Block block: i_Blocks){
            if(block.length == i_Length){
                res.add(block);
            }
        }
        return res;
    }

    private boolean isStreakOfSameValue(SupBoard.SingleStreak i_Streak, Block i_Block){
        return i_Streak.getValue() == i_Block.length && !i_Streak.isPerfect();
    }

    private int sumOfStreaksLeftOfStreak(SupBoard.SingleStreak[] i_streaks, int i_Index){
        int sum = 0;
        for(int i = 0; i < i_Index; i++){
            sum+= i_streaks[i].getValue();
        }

        return sum + i_Index;
    }

    private int sumOfStreaksRightOfStreak(SupBoard.SingleStreak[] i_streaks, int i_Index){
        int sum = 0;
        for(int i = i_Index + 1; i < i_streaks.length; i++){
            sum += i_streaks[i].getValue();
        }

        return (sum + (i_streaks.length - i_Index - 1));
    }

//    private void markPerfectBlocks(SupBoard.Streak i_Streak, CellStatus[] i_Cells, Orientation i_Orientation, int i_Id) {
//        ArrayList<Block> blocks = buildBlocks(i_Cells);
//
//        HashMap<Integer, Integer> blockCountPerLength = getLengthCounts(blocks);
//        HashMap<Integer, Integer> streakCountPerLength = getLengthStreaks(i_Streak.getStreakLengths());
//
//        blockCountPerLength.forEach((length, blocksCount) -> {
//            if (streakCountPerLength.containsKey(length)) {
//                if (streakCountPerLength.get(length).equals(blocksCount)) {
//                    markBlocksAsPerfect(blocks, length, i_Orientation, i_Id);
//                }
//            }
//        });
//    }

//    private void markBlocksAsPerfect(ArrayList<Block> i_Blocks, Integer i_Length, Orientation i_Orientation, int i_Id) {
//        SupBoard.Streaks streaks = i_Orientation == Orientation.Row ? m_Board.getRowStreaks() : m_Board.getColStreaks();
//        SupBoard.Streak streak = streaks.getStreaks().get(i_Id);
//        streak.getPerfectStreaks().add(i_Length);
//        System.out.println(i_Orientation.toString() + " : " + i_Id + " : " + i_Length);
//    }

    private HashMap<Integer, Integer> getLengthStreaks(SupBoard.SingleStreak[] i_StreakLengths) {
        HashMap<Integer, Integer> lengthCounts = new HashMap<>();

        for(SupBoard.SingleStreak streak : i_StreakLengths) {
            if (!lengthCounts.containsKey(streak.getValue())) {
                lengthCounts.put(streak.getValue(), 0);
            }
            int currVal = lengthCounts.get(streak.getValue());
            lengthCounts.put(streak.getValue(), ++currVal);
        }
        return lengthCounts;
    }

    private HashMap<Integer, Integer> getLengthCounts(ArrayList<Block> i_Blocks) {
        HashMap<Integer, Integer> lengthCounts = new HashMap<>();

        for(Block block : i_Blocks) {
            if (!lengthCounts.containsKey(block.length)) {
                lengthCounts.put(block.length, 0);
            }
            int currVal = lengthCounts.get(block.length);
            lengthCounts.put(block.length, ++currVal);
        }
        return lengthCounts;
    }

    private ArrayList<Block> buildBlocks(CellStatus[] i_Cells) {
        ArrayList<Block> blocks = new ArrayList<>();
        boolean inBlock = false;
        int currLen = 0, currStart = 0;

        for (int i = 0; i < i_Cells.length; ++i) {
            if (i_Cells[i] == CellStatus.Filled)
            {
                if (inBlock) {
                    ++currLen;
                    if(i == i_Cells.length - 1){
                        blocks.add(new Block(currStart, i, currLen));

                    }
                }
                else {
                    // not in block
                    boolean blockStart = i == 0 || i_Cells[i - 1] == CellStatus.Blank;
                    if (blockStart) {
                        inBlock = true;
                        currLen = 1;
                        currStart = i;

                        if (i == i_Cells.length - 1) {
                           blocks.add(new Block(currStart, i, currLen));
                        }
                    }
                }

            }
            else if (i_Cells[i] == CellStatus.Blank) {
                if (inBlock) {
                    blocks.add(new Block(currStart, i-1, currLen));
                    inBlock = false;
                    // data is reset on block start
                }
            }
            else { // undefined
                if (inBlock) {
                    inBlock = false;
                }
            }
        }

        return blocks;
    }

    private boolean playerWon(){
        return m_StatisticsManager.getScore() == 100;
    }

    protected abstract void HandleWin();

    public MoveInfo GetRandomMoveInfo() {
        MoveInfo randomMoveInfo = new MoveInfo();

        CellStatus randomCellStatus = CellStatus.getRandom();
        Orientation randomOrientation = Orientation.getRandom();
        int randomLineNumber = m_Board.getRandomLineNumber(randomOrientation); // index is REAL (starting from 0, not 1)
        int[] randomBlock = m_Board.getRandomCellsRange(randomOrientation);


        for (int i = 0; i < randomBlock.length; ++i) {
            if (randomOrientation == Orientation.Row) {
                randomMoveInfo.AddCellInfo(new CellInfo(randomLineNumber, randomBlock[i], randomCellStatus));
            }
            else {
                randomMoveInfo.AddCellInfo(new CellInfo(randomBlock[i], randomLineNumber, randomCellStatus));
            }
        }

        System.out.println(randomMoveInfo.toString());
        return randomMoveInfo;
    }

    @Nullable
    protected MoveInfo UndoMove() {
        MoveInfo lastMove = m_MovesLog.HandleAndLogUndo();

        if (lastMove != null) {
            m_Board.DoMove(lastMove, MoveType.Undo);
            m_StatisticsManager.incrementNumOfUndoMoves();
            m_StatisticsManager.decrementNumOfMoves();
            checkPerfectBlocks();
            checkVictory();
        }

        return lastMove;
    }

    @Nullable
    protected MoveInfo RedoMove() {
        MoveInfo moveToRedo = m_MovesLog.HandleAndLogRedo();

        if (moveToRedo != null) {
            m_Board.DoMove(moveToRedo, MoveType.Redo);
            m_StatisticsManager.incrementNumOfMoves();
            m_StatisticsManager.decrementNumOfUndoMoves();
            checkPerfectBlocks();
            checkVictory();
        }

        return moveToRedo;
    }

    private void checkVictory(){
        if(playerWon()){
            onPlayerWon();
        }
    }

    public void Reset() {
        m_PlayerWon = false;
        m_IsActiveGameSession = false;
        m_MovesLog.Reset();
        m_Board.Reset();
    }

    private void onPlayerWon(){
        m_PlayerWon = true;
        m_IsActiveGameSession = false;
        HandleWin();
//        Reload();
    }

    protected void DoComputerGame() {
        for (int i = 0; i < k_MaxNumOfComputerMoves && m_IsActiveGameSession; ++i) {
            DoMove(GetRandomMoveInfo());
            OnComputerMoveCompleted();
            checkVictory();
        }

        // if computer hasn't won, but finished his game
        if (m_IsActiveGameSession) {
            OnComputerFinished();
        }
    }

    public void DoComputerMove() {
        DoMove(GetRandomMoveInfo());
        OnComputerMoveCompleted();
    }

    protected void OnComputerFinished() {
        m_IsActiveGameSession = false;
    }

    protected void OnComputerMoveCompleted() {
    }

    public MovesLog getMovesLog() {
        return m_MovesLog;
    }
}
