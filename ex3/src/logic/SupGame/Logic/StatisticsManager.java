package logic.SupGame.Logic;

import javafx.beans.property.SimpleIntegerProperty;

import java.util.Date;
/**
 * Created by Hagai on 18/08/2016.
 *
 */

public class StatisticsManager {

    private SupBoard m_Board;
    private Solution m_Solution;

    private Date m_GameSessionStart;
    private int m_NumOfMoves;
    private int m_NumOfUndoMoves;

    private SimpleIntegerProperty m_ScoreProperty;

    public StatisticsManager(SupBoard i_Board, Solution i_Solution, Date i_CreationTime) {
        m_Board = i_Board;
        m_Solution = i_Solution;
        m_GameSessionStart = i_CreationTime;
        m_ScoreProperty = new SimpleIntegerProperty();
    }

    public SimpleIntegerProperty scoreProperty() {
        return m_ScoreProperty;
    }

    public float getScore(){
        m_ScoreProperty.setValue(calculateScore());
        return m_ScoreProperty.getValue();
    }

    private float calculateScore(){
        float score;
        int numOfCorrectCells = 0;

        for(int row = 0; row < m_Board.getNumOfRows(); ++row) {
            for(int col = 0; col < m_Board.getNumOfCols(); ++col) {
                CellStatus currCellStatus = m_Board.getCells()[row][col];

                if (currCellStatus == m_Solution.getCorrectCellStatus(row, col)) {
                    ++numOfCorrectCells;
                }
            }
        }

        int totalNumOfCells = m_Board.getNumOfRows() * m_Board.getNumOfCols();

        score = ((float)numOfCorrectCells / (float)totalNumOfCells) * 100;

        return score;
    }

    public int getNumOfMoves() {
        return m_NumOfMoves;
    }

    public int incrementNumOfMoves() {
        return m_NumOfMoves++;
    }

    public int decrementNumOfMoves() {
        return m_NumOfMoves--;
    }

    public int getNumOfUndoMoves() {
        return m_NumOfUndoMoves;
    }

    public int incrementNumOfUndoMoves() {
        return m_NumOfUndoMoves++;
    }

    public int decrementNumOfUndoMoves() {
        return m_NumOfUndoMoves--;
    }

    public long getElapsedTimeFromGameSessionStart() {
        long elapsedMiliSeconds = new Date().getTime() - m_GameSessionStart.getTime();
        long elapsedSeconds = elapsedMiliSeconds / 1000;

        return elapsedSeconds;
    }

    public void Reset() {
        m_GameSessionStart = new Date();
    }
}
