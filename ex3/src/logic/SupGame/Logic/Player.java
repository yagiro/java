package logic.SupGame.Logic;

import logic.GameInfrastructure.PlayerType;
import logic.ServerLogic.Models.UserModel;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.math.BigInteger;

/**
 * Created by yakir on 7/27/16.
 *
 */

public class Player {
    private BigInteger m_Id;
    private SimpleStringProperty m_Name;
    private PlayerType m_Type;
    private Integer m_MovesLeft;
    private SimpleBooleanProperty m_IsComputerProperty;
    private SimpleBooleanProperty m_IsPlayingProperty;

    public Player(logic.Generated.Player i_RawPlayer) throws GameDescriptorException {

        try {
            m_Id = i_RawPlayer.getId();
            m_Name = new SimpleStringProperty(i_RawPlayer.getName());
            m_Type = PlayerType.Parse(i_RawPlayer.getPlayerType());
            m_IsComputerProperty = new SimpleBooleanProperty(m_Type == PlayerType.Computer);
            m_IsPlayingProperty = new SimpleBooleanProperty(false);

        }
        catch (Exception e) {
            throw new GameDescriptorException("One of the players is not valid.");
        }
    }

    public Player(UserModel i_UserModel) throws GameDescriptorException {

        try {
            m_Name = new SimpleStringProperty(i_UserModel.getUsername());
            m_Type = i_UserModel.getPlayerType();
            m_IsComputerProperty = new SimpleBooleanProperty(m_Type == PlayerType.Computer);
            m_IsPlayingProperty = new SimpleBooleanProperty(false);

        }
        catch (Exception e) {
            throw new GameDescriptorException("One of the players is not valid.");
        }
    }

    public String getName() {
        return m_Name.get();
    }

    public PlayerType getType() {
        return m_Type;
    }

    public SimpleBooleanProperty IsComputerProperty() {
        return m_IsComputerProperty;
    }
    public SimpleBooleanProperty IsPlayingProperty() {
        return m_IsPlayingProperty;
    }

    public BigInteger getId() {
        return m_Id;
    }
}
