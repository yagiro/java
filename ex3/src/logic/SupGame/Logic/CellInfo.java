package logic.SupGame.Logic;

/**
 * Created by yakir on 8/2/16.
 *
 */

public class CellInfo {

    private int m_Row, m_Col;
    private CellStatus m_CellStatusNew;
    private CellStatus m_CellStatusOld;

    public CellInfo(int i_Row, int i_Col, CellStatus i_CellStatus) {
        m_Row = i_Row;
        m_Col = i_Col;
        m_CellStatusNew = i_CellStatus;
    }

    public int getRow() {
        return m_Row;
    }

    public int getCol() {
        return m_Col;
    }

    public CellStatus getCellStatusNew() {
        return m_CellStatusNew;
    }

    public void setCellStatusOld(CellStatus i_CellStatusBefore) {
        m_CellStatusOld = i_CellStatusBefore;
    }


    @Override
    public String toString() {
        // returns the INCREMENTED indices of row & col
        return "CellInfo { " +
                "Row = " + (m_Row + 1) +
                ", Col = " + (m_Col + 1) +
                ", CellStatus = " + m_CellStatusNew +
                " }";
    }

    public String toStringShort() {
        // returns the INCREMENTED indices of row & col
        return m_CellStatusNew.getChar() + "[" + (m_Row + 1) + ", " + (m_Col + 1) + "]";
    }

    public CellStatus getCellStatusOld() {
        return m_CellStatusOld;
    }

    public boolean HasSameCoords(CellInfo i_CellInfo) {
        int row = i_CellInfo.getRow();
        int col = i_CellInfo.getCol();

        return this.getRow() == row && this.getCol() == col;
    }

    public void setNewCellStatus(CellStatus i_CellStatus) {
        m_CellStatusNew = i_CellStatus;
    }
}
