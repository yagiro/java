package logic.SupGame.Logic;

import java.text.ParseException;

/**
 * Created by yakir on 9/20/16.
 *
 */

public enum GameType {
    SinglePlayer, MultiPlayer, DynamicMultiPlayer;

    public static GameType Parse(String i_ToParse) throws ParseException {
        i_ToParse = i_ToParse.toLowerCase();
        switch (i_ToParse) {
            case "singleplayer":
                return GameType.SinglePlayer;
            case "multiplayer":
                return GameType.MultiPlayer;
            case "dynamicmultiplayer":
                return GameType.DynamicMultiPlayer;
            default:
                throw new ParseException(i_ToParse, 0);
        }
    }
}
