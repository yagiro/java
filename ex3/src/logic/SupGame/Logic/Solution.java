package logic.SupGame.Logic;

import logic.Generated.Square;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yakir on 8/18/16.
 *
 */

public class Solution {
    private ArrayList<CellCoord> m_FilledCellCoords;

    public Solution() {
        m_FilledCellCoords = new ArrayList<>();
    }

    // Solution's constructor is initially validating the generated xml solution object before instantiating.
    // If a certain validation checks fails, the constructor throws the relevant GameDescriptor Exception
    public Solution(logic.Generated.GameDescriptor.Board i_RawBoard) throws GameDescriptorException {
        this();
        int numOfRows, numOfCols;
        try {
            numOfRows = i_RawBoard.getDefinition().getRows().intValue();
            numOfCols = i_RawBoard.getDefinition().getColumns().intValue();
        }
        catch(Exception e) {
            throw new GameDescriptorException("Game descriptor is missing a definition of the number of rows or columns.");
        }

        List<Square> squares;
        try {
            squares = i_RawBoard.getSolution().getSquare();
        }
        catch (Exception e) {
            throw new GameDescriptorException("Game descriptor is missing a definition of the solution.");
        }
        for (logic.Generated.Square square : squares) {
            // Here we first validate if each square is within board boundaries
            if(square.getRow().intValue() > numOfRows || square.getColumn().intValue() > numOfCols ){
                throw new GameDescriptorException("The square coordinates of: " + square.getRow() + "," + square.getColumn()
                        + " is out of board's boundaries!");
            }

            CellCoord CurrentCellToAdd = new CellCoord(square.getRow().intValue() - 1, square.getColumn().intValue() - 1);

            if(!m_FilledCellCoords.contains(CurrentCellToAdd)) {

                m_FilledCellCoords.add(CurrentCellToAdd);
            }
            else{
                throw new GameDescriptorException("The square coordinates of: " + square.getRow() + "," + square.getColumn()
                        + " appears more than once in the xml file's solution");
            }
        }
    }

    public ArrayList<CellCoord> getCellCoords() {
        return m_FilledCellCoords;
    }

    public CellStatus getCorrectCellStatus(int i_Row, int i_Col) {
        CellCoord cellCoord = new CellCoord(i_Row, i_Col);
        CellStatus correctStatus;

        correctStatus = this.containsCoords(cellCoord) ? CellStatus.Filled : CellStatus.Blank;

        return correctStatus;
    }

    private boolean containsCoords(CellCoord i_Cell){
        boolean coordsContained = false;
        for(CellCoord currentCell : m_FilledCellCoords){
            if(currentCell.EqualsByValue(i_Cell)){
                coordsContained = true;
                break;
            }
        }
        return coordsContained;
    }

}