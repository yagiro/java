package logic.SupGame.Logic;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import logic.Generated.GameDescriptor;
import logic.Generated.Slices;
import logic.Generated.Slice;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Logic.Exceptions.StreakException;

import java.util.*;

/**
 * Created by yakir on 7/27/16.
 *
 */

public class SupBoard {

    private final int k_MinNumOfRows = 10;
    private final int k_MinNumOfCols = 10;
    private final int k_MaxNumOfRows = 99;
    private final int k_MaxNumOfCols = 99;

    private CellStatus[][] m_Cells;
    private Streaks m_RowStreaks, m_ColStreaks;

    public SupBoard() {
        m_RowStreaks = new Streaks();
        m_ColStreaks = new Streaks();
    }

    // SupBoard's constructor is initially validating the generated xml board object before instantiating.
    // If a certain validation check fails, the constructor throws the relevant GameDescriptor Exception
    public SupBoard(GameDescriptor.Board i_RawBoard) throws GameDescriptorException {
        this();

        int numOfRows, numOfCols;
        try {
            numOfRows = i_RawBoard.getDefinition().getRows().intValue();
            numOfCols = i_RawBoard.getDefinition().getColumns().intValue();
        }
        catch(Exception e){
            throw new GameDescriptorException("Game descriptor is missing a definition of the number of rows or columns.");
        }

        validateBoardSize(numOfRows, numOfCols);
        m_Cells = new CellStatus[numOfRows][numOfCols];
        setCellsAsUndefined(numOfRows, numOfCols);
        populateStreaks(i_RawBoard.getDefinition().getSlices(), numOfRows, numOfCols);
    }

    private void validateBoardSize(int i_NumOfRows, int i_NumOfCols) throws GameDescriptorException{
        if (i_NumOfRows > k_MaxNumOfRows || i_NumOfCols > k_MaxNumOfCols) {
            throw new GameDescriptorException("Board size cannot be larger than " + k_MaxNumOfRows + " x " + k_MaxNumOfCols+".");
        }

        if (i_NumOfRows < k_MinNumOfRows || i_NumOfCols < k_MinNumOfCols) {
            throw new GameDescriptorException("Board size cannot be smaller than " + k_MinNumOfRows + " x " + k_MinNumOfCols + ".");
        }
    }

    private void populateStreaks(Slices i_Slices, int i_NumOfRows, int i_NumOfCols) throws GameDescriptorException {
        m_RowStreaks.setOrientation(Orientation.Row);
        m_ColStreaks.setOrientation(Orientation.Column);
        m_RowStreaks.GetStreaks(i_Slices);
        m_ColStreaks.GetStreaks(i_Slices);
        validateStreaks(i_NumOfRows, i_NumOfCols);
    }

    private void validateStreaks(int i_NumOfRows, int i_NumOfCols) throws StreakException, GameDescriptorException{
        validateStreaks(m_RowStreaks, i_NumOfCols);
        validateStreaks(m_ColStreaks, i_NumOfRows);

        if (m_RowStreaks.getStreaks().size() != i_NumOfRows) {
            throw new GameDescriptorException("Number of row slices is not equal to the board number of rows");
        }
        if (m_ColStreaks.getStreaks().size() != i_NumOfCols) {
            throw new GameDescriptorException("Number of column slices is not equal to the board number of columns");
        }
    }

    private void validateStreaks(Streaks i_Streaks, int i_Limit) throws StreakException {
        for(Streak streak : i_Streaks.getStreaks()){
            int sumOfBlocksAndSpaces = 0;
            for(SingleStreak block : streak.getStreakLengths()){
                sumOfBlocksAndSpaces += block.getValue();
            }
            sumOfBlocksAndSpaces += streak.getStreakLengths().length - 1;

            if(sumOfBlocksAndSpaces > i_Limit){
                throw new StreakException(streak.getId(), i_Streaks.getOrientation(),
                        i_Streaks.getOrientation() + " " + streak.getId() + " is not suitable for board size");
            }
        }
    }

    public Streaks getRowStreaks() {
        return m_RowStreaks;
    }

    public Streaks getColStreaks() {
        return m_ColStreaks;
    }

    private void createBoardAndSetUndefined(int i_Rows, int i_Cols) {
        setCellsAsUndefined(i_Rows, i_Cols);
    }

    public Integer getNumOfRows() {
        return m_RowStreaks.getStreaks().size();
    }

    public Integer getNumOfCols() {
        return m_ColStreaks.getStreaks().size();
    }

    public CellStatus[][] getCells() {
        return m_Cells;
    }

    public void DoMove(MoveInfo i_MoveInfo) {
        DoMove(i_MoveInfo, MoveType.Normal);
    }

    public void DoMove(MoveInfo i_MoveInfo, MoveType i_MoveType) {
        i_MoveInfo.getCellInfos().forEach(cellInfo -> {
            int row = cellInfo.getRow();
            int col = cellInfo.getCol();

            switch (i_MoveType) {
                case Normal:
                    cellInfo.setCellStatusOld(m_Cells[row][col]);
                    m_Cells[row][col] = cellInfo.getCellStatusNew();
                    break;
                case Undo:
                    m_Cells[row][col] = cellInfo.getCellStatusOld();
                    break;
                case Redo:
                    m_Cells[row][col] = cellInfo.getCellStatusNew();
                    break;
            }
        });
    }

    public void Reset() {
        setCellsAsUndefined(getNumOfRows(), getNumOfCols());
    }

    private void setCellsAsUndefined(int i_Rows, int i_Cols) {
        for (int i = 0; i < i_Rows; ++i) {
            for (int j = 0; j < i_Cols; ++j) {
                m_Cells[i][j] = CellStatus.Undefined;
            }
        }
    }

    public boolean IsValidMove(MoveInfo i_MoveInfo) {
        boolean isValidMove = true;

        for(CellInfo cellInfo : i_MoveInfo.getCellInfos()) {

            boolean rowOutOfBound = cellInfo.getRow() >= m_Cells.length;
            boolean colOutOfBound = cellInfo.getCol() >= m_Cells[0].length;

            if (rowOutOfBound || colOutOfBound) {
                isValidMove = false;
                break;
            }
        }

        return isValidMove;
    }

    public int getRandomLineNumber(Orientation i_RandomOrientation) {
        Random rand = new Random();
        int maxValue = i_RandomOrientation == Orientation.Row ? getNumOfRows() : getNumOfCols();
        return rand.nextInt(maxValue);
    }

    public int[] getRandomCellsRange(Orientation i_RandomOrientation) {
        Random rand = new Random();
        int[] randomCells;
        int maxValue = i_RandomOrientation == Orientation.Row ? getNumOfCols() : getNumOfRows();

        int startCell = rand.nextInt(maxValue);
        int endCell;

        if (startCell < maxValue) {
            endCell = rand.nextInt(maxValue - startCell) + startCell;
        }
        else {
            endCell = startCell;
        }

        randomCells = new int[endCell - startCell + 1];
        for (int i = 0; i < randomCells.length; ++i) {
            randomCells[i] = startCell + i;
        }

        return randomCells;
    }


    public class Streaks {
        private ArrayList<Streak> m_Streaks;
        private Orientation m_Orientation;

        public Streaks() {
            m_Streaks = new ArrayList<>();
        }

        public void setOrientation(Orientation i_Orientation) {
            this.m_Orientation = i_Orientation;
        }

        public void GetStreaks(Slices i_Slices) throws GameDescriptorException {
            getStreaks(i_Slices, m_Orientation);
        }

        private void getStreaks(Slices i_Slices, Orientation i_Orientation) throws GameDescriptorException {
            String sliceBlocksStr;
            List<Slice> slices = i_Slices.getSlice();
            for (Slice slice : slices) {
                try {
                    if (Orientation.Parse(slice.getOrientation()).equals(i_Orientation)) {
                        try {
                            sliceBlocksStr = slice.getBlocks().replaceAll("\\s+", "");
                        }
                        catch (Exception e) {
                            throw new GameDescriptorException("is lacking blocks definition.");
                        }
                        m_Streaks.add(new Streak(sliceBlocksStr, slice.getId().intValue()));

                    }
                }
                catch(GameDescriptorException gde){
                    throw new StreakException(slice.getId().intValue(), i_Orientation,
                            i_Orientation + " (ID: " + slice.getId() + ") " + gde.getMessage());
                }
            }

            Collections.sort(m_Streaks, (i_Streak1, i_Streak2) -> {
                return i_Streak1.getId() - i_Streak2.getId();
            });

        }

        public int getMaxStreakLength(){
            int max = 0;
            for(Streak streak : this.getStreaks()){
                if(max < streak.getStreakLengths().length){
                    max = streak.getStreakLengths().length;
                }
            }

            return max;
        }

        public ArrayList<Streak> getStreaks() {
            return m_Streaks;
        }


        public Orientation getOrientation() {
            return m_Orientation;
        }
    }

        private int m_Id;
        public class Streak {

            private SingleStreak[] m_StreakLengths;


            public Streak(int i_Length) {
            m_StreakLengths = new SingleStreak[i_Length];
        }

        public Streak(String i_StreakStr, int i_Id) throws GameDescriptorException {
            // i_StreakStr has been validated
            m_Id = i_Id;
            String[] streakLengthsStrings = i_StreakStr.split(",");
            m_StreakLengths = new SingleStreak[streakLengthsStrings.length];
            initStrekLengths();

            try {
                int i = 0;
                for (String lengthStr : streakLengthsStrings) {
                    m_StreakLengths[i] = new SingleStreak(Integer.parseInt(lengthStr));
//                    m_StreakLengths[i].setValue(Integer.parseInt(lengthStr));
                    ++i;
                }
            }
            catch(Exception e){
                throw new GameDescriptorException("has invalid values.");
            }
        }

        public SingleStreak[] getStreakLengths() {
            return m_StreakLengths;
        }

        public int getId() {
            return m_Id;
        }

        private void initStrekLengths(){
            for(SingleStreak streak : m_StreakLengths){
                streak = new SingleStreak();
            }
        }


    }

    public static class SingleStreak {
        private int m_Value;
        private boolean m_IsPerfect;

        public SingleStreak(){}


        public SingleStreak(int i_Value){
            m_Value = i_Value;
        }

        public SingleStreak(int i_Value, boolean i_IsPerfect){
            this(i_Value);
            m_IsPerfect = i_IsPerfect;
        }

        public int getValue() {
            return m_Value;
        }

        public void setValue(int value) {
            this.m_Value = value;
        }

        public boolean isPerfect() {
            return m_IsPerfect;
        }

        public void setPerfect(boolean perfect) {
            m_IsPerfect = perfect;
        }
    }
}
