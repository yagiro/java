package logic.SupGame.Logic;

/**
 * Created by hagai on 10/27/16.
 *
 */

public class Block {
    public int left;
    public int right;
    public int length;
    public boolean isPerfect;

    public Block(int left, int right, int length) {
        this.left = left;
        this.right = right;
        this.length = length;
        isPerfect = false;
    }
}
