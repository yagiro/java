package logic.SupGame.Logic;

import java.util.Random;

/**
 * Created by yakir on 8/2/16.
 */
public enum CellStatus {
    Filled {
        @Override
        public String getConsoleRepresentaion() {
            return " # ";
        }
        public String getChar() {
            return "F";
        }
    }, Blank {
        @Override
        public String getConsoleRepresentaion() {
            return "   ";
        }
        public String getChar() {
            return "B";
        }
    }, Undefined {
        @Override
        public String getConsoleRepresentaion() {
            return " . ";
        }
        public String getChar() {
            return "U";
        }
    };

    public abstract String getConsoleRepresentaion();
    public abstract String getChar();

    public static CellStatus getRandom() {
        Random rand = new Random();
        int randomIndex = rand.nextInt(CellStatus.values().length);
        return CellStatus.values()[randomIndex];
    }

    public static CellStatus Parse(String i_ToParse) {
        i_ToParse = i_ToParse.toLowerCase();
        switch(i_ToParse) {
            case "filled":
                return CellStatus.Filled;
            case "blank":
                return CellStatus.Blank;
            case "undefined":
                return CellStatus.Undefined;
            default:
                throw new RuntimeException("Can't parse \'"+ i_ToParse +"\' to a CellStatus.");
        }
    }
}
