package logic.SupGame.Logic;

import logic.SupGame.Logic.Exceptions.GameDescriptorException;

import java.util.Random;

/**
 * Created by yakir on 8/9/16.
 *
 */
public enum Orientation {
    Row{
        @Override
        public String toString(){
            return "Row";
        }
    }, Column {
        @Override
        public String toString(){
            return "Column";
        }
    };


    public abstract String toString();

    public static Orientation Parse(String i_ToParse) throws GameDescriptorException {
        i_ToParse = i_ToParse.toLowerCase();

        switch (i_ToParse) {
            case "row":
                return Orientation.Row;
            case "column":
                return Orientation.Column;
            default:
                throw new GameDescriptorException("has invalid orientation: (\"" + i_ToParse + "\").");
        }
    }

    public static Orientation getRandom() {
        Random rand = new Random();
        int randomIndex = rand.nextInt(Orientation.values().length);
        return Orientation.values()[randomIndex];
    }
}
