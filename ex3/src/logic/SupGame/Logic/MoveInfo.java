package logic.SupGame.Logic;

import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yakir on 8/2/16.
 *
 */

public class MoveInfo {

    private static final String s_MovePatternString;
    private static Pattern s_MovePattern;

    private static final String s_MoveBulkPatternString;
    private static Pattern s_MoveBulkPattern;

    private static Matcher s_Matcher;

    static{
        s_MovePatternString = "^([RC])(\\d+),(\\d+|\\d+:\\d+),([FBU])$";
        s_MovePattern = Pattern.compile(s_MovePatternString);

        s_MoveBulkPatternString = "^([^\\/]+)(\\/(.*))?$"; // example:  r3,5:8,f; c4,7,b / comment
        s_MoveBulkPattern = Pattern.compile(s_MoveBulkPatternString);
    }

    private ArrayList<CellInfo> m_CellInfos;
    private String m_Comment;

    public MoveInfo() {
        m_CellInfos = new ArrayList<>();
    }

    public MoveInfo(ArrayList<CellInfo> i_CellInfos){
        m_CellInfos = i_CellInfos;
    }

    public ArrayList<CellInfo> getCellInfos() {
        return m_CellInfos;
    }

    public void AddCellInfo(CellInfo i_CellInfo) {
        m_CellInfos.add(i_CellInfo);
    }

    public void AddCellInfo(int i_Row, int i_Col, CellStatus i_CellStatus) {
        m_CellInfos.add(new CellInfo(i_Row, i_Col, i_CellStatus));
    }

    public void AddCellInfoBlock(ArrayList<CellInfo> i_CellInfoArr){

        m_CellInfos.forEach(oldCellInfo -> {
            if (!containsCoords(i_CellInfoArr, oldCellInfo)) {
                i_CellInfoArr.add(oldCellInfo);
            }
        });

        m_CellInfos = i_CellInfoArr;


//        m_CellInfos.addAll(i_CellInfoArr);
    }

    private boolean containsCoords(ArrayList<CellInfo> i_CellInfoArr, CellInfo i_CellInfo) {
        boolean containsCoords = false;

        for(CellInfo cellInfo : i_CellInfoArr) {
            if (cellInfo.HasSameCoords(i_CellInfo)) {
                containsCoords = true;
                break;
            }
        }

        return containsCoords;
    }

    @Override
    public String toString() {
        return concatCellInfosAndComment();
    }

    private String concatCellInfosAndComment() {

        String concatRes = "";

        for(CellInfo cellInfo : m_CellInfos) {
            concatRes = concatRes.concat(cellInfo.toStringShort() + ", ");
        }

        // trim the last ", ";
        concatRes = concatRes.substring(0, concatRes.length() - 2);

        // add the comment (if exists)
        if (m_Comment != null) {
            concatRes = concatRes.concat(" / " + m_Comment);
        }

        concatRes = concatRes.concat(System.getProperty("line.separator"));

        return concatRes;
    }


    @Nullable
    public static MoveInfo Parse(String i_ToParse) {
        // creates and returns a MoveInfo object, parsed from a string.
        // returns null if the string is not valid.

        // i_ToParse can hold MULTIPLE moves, separated by ';' AND an OPTIONAL COMMENT denoted by '/'.
        String moveComment = parseMoveBulkComment(i_ToParse);
        i_ToParse = removeComment(i_ToParse);

        i_ToParse = i_ToParse.replaceAll("\\s+", "").toUpperCase(); // UPPERCASE and remove whitespace

        MoveInfo resMoveInfo = new MoveInfo();
        String[] moveStrings = i_ToParse.split(";");

        for (String moveString : moveStrings) {
            if (isValidMove(moveString)) {
                resMoveInfo.AddCellInfoBlock(parseSingleMove(moveString));
            } else {
                return null;
            }
        }

        resMoveInfo.m_Comment = moveComment;

        return resMoveInfo;
    }

    @Nullable
    private static String removeComment(String i_MovesBulk) {
        s_Matcher = s_MoveBulkPattern.matcher(i_MovesBulk);
        if (s_Matcher.matches()) {
            return s_Matcher.group(1);
        }

        return null;
    }

    @Nullable
    private static String parseMoveBulkComment(String i_BulkToParse) {
        String comment = null;

        s_Matcher = s_MoveBulkPattern.matcher(i_BulkToParse);
        if (s_Matcher.matches()) {
            comment = s_Matcher.group(3);
        }

        return comment;
    }

    private static boolean isValidMove(String moveString) {
        // move syntax example: "R3,4:7,B;C2,5,F / Optional comment"

        s_Matcher = s_MovePattern.matcher(moveString.toUpperCase().replaceAll("\\s+", ""));

        return s_Matcher.matches();
    }

//    private static boolean isValidRange(String i_Range){
//
//        if (isCellsRange(i_Range)) {
//            String[] rangeSplitter = i_Range.split(":");
//            Integer from = Integer.parseInt(rangeSplitter[0]);
//            Integer to = Integer.parseInt(rangeSplitter[1]);
//            return from <= to;
//        }
//
//        return true;
//    }

    private static ArrayList<CellInfo> parseSingleMove(String i_MoveString) {
        //moveString has already been validated and UPPERCASED

        s_Matcher = s_MovePattern.matcher(i_MoveString);
        s_Matcher.matches();    //groups are all mismatched without calling the matches()/find() method..

        String lineType = s_Matcher.group(1);
        String lineNumber = s_Matcher.group(2);
        String cellRange = s_Matcher.group(3);
        String cellStatus = s_Matcher.group(4);
        ArrayList<CellInfo> cellArray = new ArrayList<>();


        if (isCellsRange(cellRange)){
            cellArray.addAll(getRangeOfCells(lineType, lineNumber, cellRange, cellStatus));
        }
        else{
            if(lineType.equals("R")){
                cellArray.add(new CellInfo(Integer.parseInt(lineNumber) - 1,
                        Integer.parseInt(cellRange) - 1, getCellStatus(cellStatus)));
            }
            else{
                cellArray.add(new CellInfo(Integer.parseInt(cellRange) - 1,
                        Integer.parseInt(lineNumber) - 1, getCellStatus(cellStatus)));
            }
        }

        return cellArray;

    }

    private static ArrayList<CellInfo> getRangeOfCells(String i_lineType, String i_lineNumber, String i_cellRange, String i_status){
        // when getting range of cells we already know that this is a legal range

        int i;
        ArrayList<CellInfo> cellRange = new ArrayList<>();
        String[] rangeSplitter = i_cellRange.split(":");
        Integer lineNumber = Integer.parseInt(i_lineNumber) - 1;
        Integer fromCell = Integer.parseInt(rangeSplitter[0]) - 1;
        Integer toCell = Integer.parseInt(rangeSplitter[1]) - 1;

        if (toCell < fromCell) {
            // this enables the user to enter a range from a bigger to a smaller cell
            Integer tmp = fromCell;
            fromCell = toCell;
            toCell = tmp;
        }

        if(i_lineType.equals("R")){
            for(i = fromCell; i <= toCell; i++){
                cellRange.add(new CellInfo(lineNumber, i, getCellStatus(i_status)));
            }
        }
        // line type is "C"
        else{
            for(i = fromCell; i<=toCell; i++){
                cellRange.add(new CellInfo(i, lineNumber,getCellStatus(i_status)));
            }
        }

        return cellRange;
    }

    private static CellStatus getCellStatus(String i_Status){
        switch (i_Status){
            case "F":
                return CellStatus.Filled;
            case "B":
                return CellStatus.Blank;
            case "U":
                return CellStatus.Undefined;
            default:
                return null; //cant happen according to our logic
        }
    }


    private static boolean isCellsRange(String i_ToCheck) {
        return i_ToCheck.contains(":");
    }
}
