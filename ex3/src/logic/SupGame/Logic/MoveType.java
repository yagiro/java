package logic.SupGame.Logic;

import java.util.Random;

/**
 * Created by yakir on 8/17/16.
 *
 */

public enum MoveType {
    Normal, Undo, Redo, Skip;

//    public static MoveType getRandom() {
//        Random rand = new Random();
//        return MoveType.values()[rand.nextInt(MoveType.values().length)];
//    }

    public static MoveType getRandom() {
        Random rand = new Random();
        return MoveType.values()[rand.nextInt(MoveType.values().length - 1)]; // -1 because we want to NOT pick "Skip".
    }

    public static MoveType Parse(String i_ToParse) {
        i_ToParse = i_ToParse.toLowerCase();
        switch (i_ToParse) {
            case "normal":
                return MoveType.Normal;
            case "undo":
                return MoveType.Undo;
            case "redo":
                return MoveType.Redo;
            case "skip":
                return MoveType.Skip;
            default:
                throw new RuntimeException("Can't parse \'" + i_ToParse + "\' to a CellStatus.");
        }
    }
}
