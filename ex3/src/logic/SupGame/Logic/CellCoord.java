package logic.SupGame.Logic;

/**
 * Created by yakir on 9/12/16.
 *
 */

public class CellCoord {
    public int Row, Col;

    public CellCoord(int i_Row, int i_Col) {
        Row = i_Row;
        Col = i_Col;
    }

    public boolean EqualsByValue(CellCoord i_cell) {
        return this.Row == i_cell.Row && this.Col == i_cell.Col;
    }
}
