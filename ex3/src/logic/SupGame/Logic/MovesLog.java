package logic.SupGame.Logic;

import com.sun.istack.internal.Nullable;
import javafx.beans.property.SimpleBooleanProperty;

import java.util.Stack;

/**
 * Created by yakir on 8/16/16.
 *
 */

public class MovesLog {
    private Stack<MoveInfo> m_MovesStack;
    private Stack<MoveInfo> m_UndoneMovesStack;
    private SimpleBooleanProperty m_HasMovesToUndoProperty;
    private SimpleBooleanProperty m_HasMovesToRedoProperty;

    public MovesLog() {
        m_MovesStack = new Stack<>();
        m_UndoneMovesStack = new Stack<>();
        m_HasMovesToUndoProperty = new SimpleBooleanProperty();
        m_HasMovesToRedoProperty = new SimpleBooleanProperty();
    }

    public void HandleAndLogMove(MoveInfo i_MoveInfo) {
        m_MovesStack.push(i_MoveInfo);
        m_UndoneMovesStack.clear();
        m_HasMovesToUndoProperty.setValue(true);
        m_HasMovesToRedoProperty.setValue(false);
    }

    @Nullable
    public MoveInfo HandleAndLogUndo() {
        MoveInfo lastMove = null;

        if (!m_MovesStack.isEmpty()) {
            lastMove = m_MovesStack.pop();
            if(m_MovesStack.isEmpty()){
                m_HasMovesToUndoProperty.setValue(false);
            }
            m_UndoneMovesStack.push(lastMove);
            m_HasMovesToRedoProperty.setValue(true);
        }

        return lastMove;
    }

    @Nullable
    public MoveInfo HandleAndLogRedo() {
        MoveInfo moveToRedo = null;

        if (!m_UndoneMovesStack.isEmpty()) {
            moveToRedo = m_UndoneMovesStack.pop();
            if(m_UndoneMovesStack.isEmpty()){
                m_HasMovesToRedoProperty.setValue(false);
            }
            m_MovesStack.push(moveToRedo);
            m_HasMovesToUndoProperty.setValue(true);
        }

        return moveToRedo;
    }

    public boolean CanRedo(){
        return !m_UndoneMovesStack.isEmpty();
    }

    public final Stack<MoveInfo> getStack()
    {
        return m_MovesStack;
    }

    public void Reset() {
        m_MovesStack.clear();
        m_UndoneMovesStack.clear();
    }

    public Stack<MoveInfo> ReturnOrderedMovelogs(){
        Stack<MoveInfo> OrderedStack = new Stack<>();
        for(MoveInfo move : m_MovesStack){
            OrderedStack.push(move);
        }
        return OrderedStack;
    }

    public SimpleBooleanProperty HasMovesToUndoProperty() {
        return m_HasMovesToUndoProperty;
    }

    public SimpleBooleanProperty HasMovesToRedoProperty() {
        return m_HasMovesToRedoProperty;
    }
}
