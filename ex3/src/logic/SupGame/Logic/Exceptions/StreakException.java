package logic.SupGame.Logic.Exceptions;

import logic.SupGame.Logic.Orientation;

/**
 * Created by Hagai on 18/08/2016.
 *
 */

public class StreakException extends GameDescriptorException {

    private int m_StreakId;
    private Orientation m_Orientation;

    public StreakException() {}

    public StreakException(int i_Id, Orientation i_Orientation, String i_Message){
        super(i_Message);
        m_StreakId = i_Id;
        m_Orientation = i_Orientation;
    }



    public int getStreakId() {
        return m_StreakId;
    }

    public void setStreakId(int i_StreakId) {
        this.m_StreakId = i_StreakId;
    }

    public Orientation getOrientation() {
        return m_Orientation;
    }

    public void setOrientation(Orientation i_Orientation) {
        this.m_Orientation = i_Orientation;
    }
}
