package logic.SupGame.Logic.Exceptions;

/**
 * Created by yakir on 8/9/16.
 *
 */

public class GameDescriptorException extends Exception {
    public GameDescriptorException(){}
    public GameDescriptorException(String i_Message) {
        super(i_Message);
    }
}
