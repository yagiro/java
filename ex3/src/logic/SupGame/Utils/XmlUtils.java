package logic.SupGame.Utils;

import logic.Generated.GameDescriptor;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import com.sun.istack.internal.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by yakir on 8/9/16.
 *
 */

public class XmlUtils {

    @Nullable
    public static GameDescriptor ParseGameDescriptorFromXml(String i_XmlFilePath) throws GameDescriptorException {
        return ParseGameDescriptorFromXml(new File(i_XmlFilePath));
    }

    @Nullable
    public static GameDescriptor ParseGameDescriptorFromXml(File i_XmlFile) throws GameDescriptorException {
        File xmlFile;
        GameDescriptor gameDescriptor = null;

        xmlFile = i_XmlFile;

        if(!xmlFile.isFile() || !xmlFile.exists()){
            throw new GameDescriptorException("This file is not found: " + i_XmlFile.getAbsolutePath());
        }
        if (xmlFile.getAbsolutePath().endsWith(".xml")) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(GameDescriptor.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                gameDescriptor = (GameDescriptor) jaxbUnmarshaller.unmarshal(xmlFile);
            }
            catch (JAXBException e) {
                throw new GameDescriptorException("The file: " + i_XmlFile.getAbsolutePath() + " is not a valid game descriptor.");
            }
        }
        else {
            throw new GameDescriptorException("A game descriptor must be a .xml file.");
        }

        return gameDescriptor;
    }

    @Nullable
    public static GameDescriptor ParseGameDescriptorFromXml(InputStream i_InputStream) throws GameDescriptorException {
        GameDescriptor gameDescriptor = null;

        if (i_InputStream == null) {
            throw new GameDescriptorException("InputStream = null");
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(GameDescriptor.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            gameDescriptor = (GameDescriptor) jaxbUnmarshaller.unmarshal(i_InputStream);
        }
        catch (JAXBException e) {
            throw new GameDescriptorException("JAXB could not parse the game descriptor.");
        }


        return gameDescriptor;
    }
}
