package logic.GameInfrastructure;

/**
 * Created by yakir on 8/1/16.
 *
 */

public abstract class Game {
    protected boolean m_IsActive;
    public abstract void Run();
}
