package logic.GameInfrastructure;

import logic.SupGame.Logic.Exceptions.GameDescriptorException;

/**
 * Created by yakir on 8/17/16.
 *
 */

public enum PlayerType {
    Human, Computer;

    public static PlayerType Parse(String i_ToParse) throws GameDescriptorException {
        i_ToParse = i_ToParse.toLowerCase();

        switch (i_ToParse) {
            case "computer":
                return PlayerType.Computer;
            case "human":
                return PlayerType.Human;
            default:
                throw new GameDescriptorException("One of the player types is not valid.");
        }
    }
}
