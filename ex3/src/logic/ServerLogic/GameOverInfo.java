package logic.ServerLogic;

/**
 * Created by yakir on 10/30/16.
 *
 */

public class GameOverInfo {
    private String winner;
    private String reason;

    public GameOverInfo(Reason i_Reason) {
        reason = i_Reason.name();
    }

    public GameOverInfo(Reason i_Reason, String i_Winner) {
        this(i_Reason);
        winner = i_Winner;
    }

    public enum Reason {
        RoundsOver, Win, SinglePlayerLeft
    }
}
