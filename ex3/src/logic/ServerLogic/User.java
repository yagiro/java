package logic.ServerLogic;

import Utils.ServletUtils;
import logic.ServerLogic.Models.UserModel;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by yakir on 10/19/16.
 *
 */

public class User {
    private UserModel m_UserModel;
    private SupWebManager m_WebManager;

    public User() {}

    public User(String i_Username, HttpServletRequest req) {
        m_WebManager = ServletUtils.getWebManager(req);
        m_UserModel = m_WebManager.getUsersManager().getUserModel(i_Username);
    }

    public boolean JoinGame(String gameName) {
        return false;
    }

    public UserModel getUserModel() {
        return m_UserModel;
    }

    public void setUserModel(UserModel i_UserModel) {
        m_UserModel = i_UserModel;
    }
}
