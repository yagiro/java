package logic.ServerLogic;

import Utils.SessionUtils;
import logic.Generated.GameDescriptor;
import logic.ServerLogic.Exceptions.RoomNameTakenException;
import logic.ServerLogic.Exceptions.RoomClosedException;
import logic.ServerLogic.Exceptions.UserAlreadyInRoomException;
import logic.ServerLogic.Models.UserModel;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * Created by yakir on 10/19/16.
 *
 */

public class RoomsManager {

    private static final Object s_Lock;
    private static RoomsManager s_Instance;

    private HashMap<String, Room> m_Rooms;
    private HashMap<String, ShortRoomInfo> m_RoomsShortInfo;
    private int m_Version;

    static {
        s_Lock = new Object();
    }

    private RoomsManager() {
        m_Rooms = new HashMap<>();
        m_RoomsShortInfo = new HashMap<>();
        m_Version = 1;


//        Room g = new ShortRoomInfo();
//        g.setAdmin("hagai");
//        g.setBoardSize(10,20);
//        g.setRounds(23);
//        g.setName("nala");
//        g.setPlayersCount(29,2);
//        m_GameModels.add(g);
//
    }

    public static RoomsManager getInstance() {
        if (s_Instance == null) {
            synchronized (s_Lock) {
                if (s_Instance == null) {
                    s_Instance = new RoomsManager();
                }
            }
        }

        return s_Instance;
    }

    public HashMap<String, ShortRoomInfo> getRoomsInfo() {
        synchronized (s_Lock) {
            return m_RoomsShortInfo;
        }
    }

    public void JoinUserToRoom(UserModel i_UserModel, String i_RoomName, HttpServletRequest req)
            throws RoomClosedException, UserAlreadyInRoomException {
        synchronized(s_Lock) {
            Room roomToJoin = m_Rooms.get(i_RoomName);
            roomToJoin.JoinUser(i_UserModel);
            onUserJoined(i_RoomName, req);
        }
    }

    private void onUserJoined(String i_RoomName, HttpServletRequest req) {
        SessionUtils.RegisterJoin(req, i_RoomName);
        m_RoomsShortInfo.get(i_RoomName).IncrementAvailablePlayers();
        ++m_Version;
    }

    public Room getRoom(String i_RoomName) {
        synchronized(s_Lock) {
            return m_Rooms.get(i_RoomName);
        }
    }

    public Room CreateRoom(GameDescriptor i_GameDescriptor, String i_Admin)
            throws GameDescriptorException, RoomNameTakenException {

        synchronized (s_Lock) {

            Room newRoom = null;

            if (i_GameDescriptor.getDynamicMultiPlayers() == null) {
                throw new GameDescriptorException("Missing DynamicMultiPlayers.");
            }

            try {
                String roomName = i_GameDescriptor.getDynamicMultiPlayers().getGametitle();
                if (!m_Rooms.containsKey(roomName)) {
                    ShortRoomInfo shortRoomInfo = retrieveShortRoomInfo(i_GameDescriptor, i_Admin);
                    newRoom = new Room(shortRoomInfo, i_GameDescriptor);
                    m_Rooms.put(shortRoomInfo.getName(), newRoom);
                    m_RoomsShortInfo.put(shortRoomInfo.getName(), shortRoomInfo.getClone());
                    ++m_Version;
                }
                else {
                    throw new RoomNameTakenException();
                }

                return newRoom;
            }
            catch (RoomNameTakenException rte) {
                throw rte;
            }
            catch(GameDescriptorException gde){
                throw gde;
            }
            catch(Exception e){
                throw new GameDescriptorException("Missing GameTitle in DynamicMultiPlayers.");
            }
        }
    }

    private ShortRoomInfo retrieveShortRoomInfo(GameDescriptor i_GameDescriptor, String i_Username) throws GameDescriptorException {
        GameDescriptor.DynamicMultiPlayers dynamicMultiPlayers;
        try {
            dynamicMultiPlayers = i_GameDescriptor.getDynamicMultiPlayers();
        }
        catch (Exception e){
            throw new GameDescriptorException("Missing DynamicMultiPlayers.");
        }

        ShortRoomInfo shortRoomInfo = new ShortRoomInfo();
        try {
            shortRoomInfo.setName(dynamicMultiPlayers.getGametitle());
        }
        catch(Exception e) {
            throw new GameDescriptorException("Missing GameTitle in DynamicMultiPlayers.");
        }

        if (dynamicMultiPlayers.getGametitle().matches("^\\s*$")) {
            throw new GameDescriptorException("GameTitle must have at least one character (not whitespace).");
        }

        shortRoomInfo.setAdmin(i_Username);

        int totalMoves;
        // get rounds
        try {
            totalMoves = Integer.parseInt(dynamicMultiPlayers.getTotalmoves());
            shortRoomInfo.setRounds(totalMoves);
        }
        catch(Exception e) {
            throw new GameDescriptorException("Invalid value: TotalMoves in DynamicMultiPlayers.");
        }

        if (totalMoves <= 0) {
            throw new GameDescriptorException("Total moves must be larger than 0.");
        }

        int numOfRows, numOfColumns;
        // get board size
        try {
            numOfRows = i_GameDescriptor.getBoard().getDefinition().getRows().intValue();
            numOfColumns = i_GameDescriptor.getBoard().getDefinition().getColumns().intValue();
            shortRoomInfo.setBoardSize(numOfRows, numOfColumns);
        }
        catch(Exception e) {
            throw new GameDescriptorException("Invalid rows/cols in board definition.");
        }

        int totalPlayers;
        try {
            totalPlayers = Integer.parseInt(dynamicMultiPlayers.getTotalPlayers());
            shortRoomInfo.setPlayersCount(totalPlayers, 0);
        }
        catch (Exception e) {
            throw new GameDescriptorException("Invalid value: TotalPlayers in DynamicMultiPlayers.");
        }

        if (totalPlayers <= 0) {
            throw new GameDescriptorException("Total players must be larger than 0.");
        }

        return shortRoomInfo;
    }

    public int getVersion() {
        return m_Version;
    }

    public void RemoveUserFromRoom(String i_Username, String i_RoomName, HttpSession i_Session) {
        synchronized (s_Lock) {
            m_Rooms.get(i_RoomName).LeaveUser(i_Username);
            onUserLeft(i_RoomName, i_Session);
        }
    }

    public void RemoveUserFromRoom(String i_Username, String i_RoomName, HttpServletRequest req) {
        RemoveUserFromRoom(i_Username, i_RoomName, req.getSession());
    }

//    public void RemoveUserFromRoom(String i_Username, String i_RoomName, HttpServletRequest req) {
//        synchronized (s_Lock) {
//            m_Rooms.get(i_RoomName).LeaveUser(i_Username);
//            onUserLeft(i_RoomName, req);
//        }
//    }

    private void onUserLeft(String i_RoomName, HttpServletRequest req) {
        onUserLeft(i_RoomName, req.getSession());
    }

    private void onUserLeft(String i_RoomName, HttpSession i_Session) {
        SessionUtils.RegisterLeave(i_Session);
        m_RoomsShortInfo.get(i_RoomName).DecrementAvailablePlayers();
        ++m_Version;
    }
}
