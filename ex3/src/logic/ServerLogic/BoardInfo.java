package logic.ServerLogic;

import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;
import logic.SupGame.Logic.CellStatus;
import logic.SupGame.Logic.Solution;
import logic.SupGame.Logic.SupBoard;

import java.util.ArrayList;

/**
 * Created by Hagai on 25/10/2016.
 *
 */

public class BoardInfo {

    private ArrayList<SupBoard.Streak> rowStreaks, colStreaks;
    private CellStatus[][] boardState;

    public BoardInfo(SupBoard i_Board) {
        this(i_Board.getRowStreaks().getStreaks(), i_Board.getColStreaks().getStreaks());
        boardState = i_Board.getCells();
    }

    public  ArrayList<SupBoard.Streak> getRowStreaks() {
        return rowStreaks;
    }

    public  ArrayList<SupBoard.Streak> getColStreaks() {
        return colStreaks;
    }

    public BoardInfo(){}

    public BoardInfo(ArrayList<SupBoard.Streak> i_RowStreaks, ArrayList<SupBoard.Streak> i_ColStreaks){
        rowStreaks = i_RowStreaks;
        colStreaks = i_ColStreaks;
    }

    public void OnStartGame(JavaFxSupGame i_CurrentGame) {
        refreshData(i_CurrentGame);
    }

    public void OnMoveMade(JavaFxSupGame i_CurrentGame) {
        refreshData(i_CurrentGame);
    }

    private void refreshData(JavaFxSupGame i_CurrentGame) {
        boardState = i_CurrentGame.getBoard().getCells();
        rowStreaks = i_CurrentGame.getBoard().getRowStreaks().getStreaks();
        colStreaks = i_CurrentGame.getBoard().getColStreaks().getStreaks();
    }
}
