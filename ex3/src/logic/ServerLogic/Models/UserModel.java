package logic.ServerLogic.Models;

import logic.GameInfrastructure.PlayerType;

/**
 * Created by yakir on 10/6/16.
 *
 */

public class UserModel {
    private String username;
    private PlayerType playerType;

    public UserModel() {
        username = "[NO_USERNAME]";
    }

    public UserModel(String i_Username, PlayerType i_PlayerType) {
        username = i_Username;
        playerType = i_PlayerType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String i_Username) {
        username = i_Username;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setPlayerType(PlayerType i_PlayerType) {
        playerType = i_PlayerType;
    }

    @Override
    protected Object clone() {
        try {
            return new UserModel(username, playerType);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public UserModel getClone() {
        return (UserModel) this.clone();
    }
}
