package logic.ServerLogic.Models;

/**
 * Created by yakir on 10/15/16.
 *
 */

public class Versions {
    private int games;
    private int users;

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }
}
