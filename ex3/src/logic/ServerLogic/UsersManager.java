package logic.ServerLogic;

import logic.ServerLogic.Models.UserModel;

import java.util.ArrayList;

/**
 * Created by Hagai on 10/6/16.
 *
 */

public class UsersManager {
    private static final Object s_Lock;
    private static UsersManager s_Instance;

    private ArrayList<UserModel> m_UserModels;

    static {
        s_Lock = new Object();
    }

    private int m_Version;

    private UsersManager() {
        m_UserModels = new ArrayList<>();
        m_Version = 1;
    }

    public static UsersManager getInstance() {
        if (s_Instance == null) {
            synchronized (s_Lock) {
                if (s_Instance == null) {
                    s_Instance = new UsersManager();
                }
            }
        }

        return s_Instance;
    }

    public boolean Contains(UserModel i_UserModel) {
        synchronized (s_Lock) {
            boolean userExists = false;
            for (UserModel userModel : m_UserModels) {
                if (userModel.getUsername().equals(i_UserModel.getUsername())) {
                    userExists = true;
                    break;
                }
            }
            return userExists;
        }
    }

    public void Add(UserModel i_UserModel) {
        synchronized (s_Lock) {
            if (!this.Contains(i_UserModel)) {
                m_UserModels.add(i_UserModel);
            } else {
                throw new RuntimeException("Cannot add user. UserModel already exists.");
            }
            ++m_Version;
        }
    }

    public ArrayList<UserModel> getUsers() {
        synchronized (s_Lock) {
            ArrayList<UserModel> listClone = new ArrayList<>();

            for (UserModel user : m_UserModels) {
                listClone.add(user.getClone());
            }

            return listClone;
        }
    }

    public UserModel getUserModel(String i_Username) {
        UserModel userModelClone = null;

        synchronized (s_Lock) {
            for (UserModel user : m_UserModels) {
                if (user.getUsername().equals(i_Username)) {
                    userModelClone = user.getClone();
                    break;
                }
            }

            return userModelClone;
        }
    }

    public void Remove(String username) {
        synchronized (s_Lock) {
            UserModel toRemove = null;
            for(UserModel user : m_UserModels) {
                if (user.getUsername().equals(username)) {
                    toRemove = user;
                    break;
                }
            }
            if (toRemove != null) {
                m_UserModels.remove(toRemove);
                ++m_Version;
            }
        }
    }

    public int getVersion() {
        return m_Version;
    }
}
