package logic.ServerLogic;

import com.sun.javafx.geom.Vec2d;

/**
 * Created by yakir on 10/13/16.
 *
 */
public class ShortRoomInfo {
    protected String name;
    protected String admin;
    protected int rounds;
    protected Vec2d boardSize;
    protected PlayersModel playersCount;

    public ShortRoomInfo(){
        boardSize = new Vec2d();
        playersCount = new PlayersModel();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public Vec2d getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(int rows, int cols) {
        boardSize.set(rows, cols);
    }

    public PlayersModel getPlayersCount() {
        return playersCount;
    }

    public void setPlayersCount(PlayersModel playersCount) {
        this.playersCount = playersCount;
    }

    public void setPlayersCount(int requirePlayers, int joinedPlayers){
        playersCount.setAvailablePlayers(joinedPlayers);
        playersCount.setRequiredPlayers(requirePlayers);
    }

    public void IncrementAvailablePlayers(){
        playersCount.setAvailablePlayers(playersCount.getAvailablePlayers() + 1);
    }

    public void DecrementAvailablePlayers(){
        playersCount.setAvailablePlayers(playersCount.getAvailablePlayers() - 1);
    }

    @Override
    protected Object clone() {

        try {
            ShortRoomInfo clone = new ShortRoomInfo();

            clone.setName(name);
            clone.setAdmin(admin);
            clone.setRounds(rounds);
            clone.setBoardSize((int)boardSize.x, (int)boardSize.y);
            clone.setPlayersCount(playersCount.getRequiredPlayers(), playersCount.getAvailablePlayers());
            return clone;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ShortRoomInfo getClone() {
        return (ShortRoomInfo) this.clone();
    }

    public class PlayersModel{
        private int requiredPlayers;
        private int availablePlayers;

        public int getRequiredPlayers() {
            return requiredPlayers;
        }

        public void setRequiredPlayers(int requiredPlayers) {
            this.requiredPlayers = requiredPlayers;
        }

        public int getAvailablePlayers() {
            return availablePlayers;
        }

        public void setAvailablePlayers(int availablePlayers) {
            this.availablePlayers = availablePlayers;
        }
    }
}
