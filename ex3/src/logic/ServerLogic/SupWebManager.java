package logic.ServerLogic;

import javax.servlet.http.HttpSessionListener;

/**
 * Created by yakir on 10/15/16.
 *
 */

public class SupWebManager {
    private static SupWebManager s_Instance;
    private static Object s_Lock;

    private HttpSessionListener m_SessionListener;

    static {
        s_Lock = new Object();
    }

    private SupWebManager() {
        m_SessionListener = new SessionListener();
    }

    public static SupWebManager getInstance() {
        if (s_Instance == null) {
            synchronized (s_Lock) {
                if (s_Instance == null) {
                    s_Instance = new SupWebManager();
                }
            }
        }

        return s_Instance;
    }

    public UsersManager getUsersManager() {
        return UsersManager.getInstance();
    }

    public RoomsManager getRoomsManager() {
        return RoomsManager.getInstance();
    }
}
