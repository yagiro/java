package logic.ServerLogic;

import logic.GameInfrastructure.PlayerType;

/**
 * Created by yakir on 10/27/16.
 *
 */

public class PlayerInfo {
    private PlayerType playerType;
    private int score;

    public PlayerInfo() {}

    public PlayerInfo(PlayerType playerType) {
        this.playerType = playerType;
        this.score = 0;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
