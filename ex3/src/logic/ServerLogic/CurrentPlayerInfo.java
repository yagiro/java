package logic.ServerLogic;

import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupArcade;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;
import logic.SupGame.Logic.SupBoard;

import java.util.ArrayList;

/**
 * Created by yakir on 10/27/16.
 *
 */

public class CurrentPlayerInfo {
    public String name;
    public int move;
    private int score;
    private BoardInfo boardInfo;
    private boolean isTurnsOver;
    private boolean isWinner;

    public CurrentPlayerInfo() {
        boardInfo = new BoardInfo();
    }

    public CurrentPlayerInfo(JavaFxSupGame i_Game, Room i_Room) {
        name = i_Game.getPlayer().getName();
        move = i_Room.turnNumberProperty().getValue();
        score = i_Game.scoreProperty().getValue();
        boardInfo = new BoardInfo(i_Game.getBoard());
        isTurnsOver = i_Room.AreTurnsOverProperty().getValue();
        isWinner = score == 100;
    }

    public CurrentPlayerInfo(String userName) {
        this.name = userName;
        this.move = 0;
    }

    public void OnMove(JavaFxSupGame i_CurrentGame, int i_Move, boolean i_AreTurnsOver){
        refreshData(i_CurrentGame, i_Move, i_AreTurnsOver);
        boardInfo.OnMoveMade(i_CurrentGame);
    }

    public void OnGameStart(JavaFxSupGame i_CurrentGame) {
        boardInfo.OnStartGame(i_CurrentGame);
        refreshData(i_CurrentGame, 0, false);
    }

    private void refreshData(JavaFxSupGame i_CurrentGame, int i_Move, boolean i_AreTurnsOver) {
        name = i_CurrentGame.getPlayer().getName();
        move = i_Move;
        score = i_CurrentGame.scoreProperty().getValue();
        isTurnsOver = i_AreTurnsOver;
        isWinner = score == 100;
    }

    public BoardInfo getBoardInfo() {
        return boardInfo;
    }

    public void Reset() {
        name = "";
        move = 1;
        score = 0;
        boardInfo = new BoardInfo();
        isTurnsOver = false;
        isWinner = false;
    }
}
