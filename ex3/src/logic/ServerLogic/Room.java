package logic.ServerLogic;

import logic.GameInfrastructure.PlayerType;
import logic.Generated.GameDescriptor;
import logic.ServerLogic.Exceptions.RoomClosedException;
import logic.ServerLogic.Exceptions.UserAlreadyInRoomException;
import logic.ServerLogic.Models.UserModel;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupArcade;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Logic.MoveInfo;
import logic.SupGame.Logic.MoveType;
import logic.SupGame.Logic.SupBoard;

/**
 * Created by yakir on 10/19/16.
 *
 */

public class Room extends JavaFxSupArcade {
    private final Object m_Lock;
    private GameDescriptor m_GameDescriptor;
    private RoomInfo m_RoomInfo;
    private boolean m_IsOpen, m_IsFull;
    private CurrentPlayerInfo m_CurrentPlayer;


    public Room(ShortRoomInfo i_ShortRoomInfo, GameDescriptor i_GameDescriptor) throws GameDescriptorException {
        super();
        validateGameDescriptorAndSetRoomInfo(i_GameDescriptor, i_ShortRoomInfo); // will throw exception if not valid
        m_GameDescriptor = i_GameDescriptor;
        m_IsOpen = true;
        m_IsFull = false;
        m_RoomInfo.setHadStarted(false);
        m_Lock = new Object();
        m_CurrentPlayer = new CurrentPlayerInfo();
        InitializeMembers();
    }

    private void validateGameDescriptorAndSetRoomInfo(GameDescriptor i_GameDescriptor, ShortRoomInfo i_ShortRoomInfo)
            throws GameDescriptorException {
        UserModel dummyUser = new UserModel("validationDummy", PlayerType.Computer);
        JavaFxSupGame dummyGame = new JavaFxSupGame(dummyUser, i_GameDescriptor);
        SupBoard.Streaks rowStreaks =  dummyGame.getBoard().getRowStreaks();
        SupBoard.Streaks colStreaks =  dummyGame.getBoard().getColStreaks();
        BoardInfo boardInfo = new BoardInfo(rowStreaks.getStreaks(), colStreaks.getStreaks());
        m_RoomInfo = new RoomInfo(i_ShortRoomInfo, boardInfo);
    }

    public void JoinUser(UserModel i_UserModel)
            throws RoomClosedException, UserAlreadyInRoomException {
        synchronized (m_Lock) {
            if (m_IsOpen) {
                if (!userIsInRoom(i_UserModel)) {
                    m_Games.add(createNewGameFromValidGameDescriptor(i_UserModel));
                    onPlayerJoined(i_UserModel);
                }
                else {
                    throw new UserAlreadyInRoomException();
                }
            }
            else {
                throw new RoomClosedException();
            }
        }
    }

    private boolean userIsInRoom(UserModel i_UserModel) {
        return m_RoomInfo.getPlayers().containsKey(i_UserModel.getUsername());
    }

    private boolean isRoomFull() {
        return m_RoomInfo.getPlayersCount().getAvailablePlayers() == m_RoomInfo.getPlayersCount().getRequiredPlayers();
    }

    private void onPlayerJoined(UserModel i_UserModel) {
        m_RoomInfo.PlayerJoined(i_UserModel);
        m_IsFull = isRoomFull();
        if (m_IsFull) {
            startGame();
        }
    }

    private void startGame() {
        m_CurrentGame = m_Games.get(m_CurrentGameIndexProperty.getValue());
        super.Reload();
        m_IsOpen = false;
        m_CurrentGame.getPlayer().IsPlayingProperty().setValue(true);
        m_CurrentPlayer.OnGameStart(m_CurrentGame);
        m_RoomInfo.OnStartGame(m_Games, m_RoundNumberProperty.getValue(), m_CurrentPlayer.name);
    }

    public void LeaveUser(String i_UserName) {
        synchronized (m_Lock) {
            JavaFxSupGame gameToDelete = getGameByUserName(i_UserName);
            int index = m_Games.indexOf(gameToDelete);
            m_Games.remove(gameToDelete);

            if (m_RoomInfo.getHadStarted() && m_Games.size() > 0) {
                if (m_CurrentGameIndexProperty.getValue() >= index) {
                    m_CurrentGameIndexProperty.setValue(m_CurrentGameIndexProperty.getValue() - 1);
                    if (m_CurrentGameIndexProperty.getValue() + 1 == index) {
                        //current game is the one trying to leave
                        super.MoveToNextGame();
                        onMoveMade();
                    }
                }
            }

            onUserLeft(i_UserName);
        }
    }

    private void onAllPlayersLeftFromActiveGame() {
        reset();
    }

    private void reset() {
        m_IsOpen = true;
        m_IsFull = false;
        InitializeMembers();
        m_RoomInfo.Reset();
        m_CurrentPlayer.Reset();
    }

    public GameDescriptor getGameDescriptor() {
        return m_GameDescriptor;
    }


    private void onUserLeft(String i_UserName) {
        m_RoomInfo.PlayerLeft(i_UserName);
        m_IsFull = isRoomFull();

        if (!m_RoomInfo.getHadStarted() && !m_IsFull) {
            m_IsOpen = true;
        }

        if (m_Games.size() == 0 && m_RoomInfo.getHadStarted()) {
            onAllPlayersLeftFromActiveGame();
        }
    }

    private JavaFxSupGame getGameByUserName(String i_Username) {
        JavaFxSupGame theGame = null;
        for(JavaFxSupGame supGame : m_Games) {
            if (supGame.getPlayer().getName().equals(i_Username)) {
                theGame = supGame;
                break;
            }
        }
        return theGame;
    }

    private JavaFxSupGame createNewGameFromValidGameDescriptor(UserModel i_UserModel) {
        try {
            // this try and catch block is only to relax the compiler.
            // at this point, m_GameDescriptor was already validated.
            return new JavaFxSupGame(i_UserModel, m_GameDescriptor);
        } catch (GameDescriptorException gde) {
            // this exception will never be thrown.
        }
        return null; // never gets here.
    }

    public RoomInfo getInfo() {
        return m_RoomInfo;
    }

    public void InitializeMembers(){
        m_Games.clear();
        m_TurnsLeftProperty.setValue(k_DefaultTurnsLeft);
        m_MaxRoundsProperty.setValue(m_RoomInfo.getRounds());
        m_RoundsLeftProperty.setValue(m_MaxRoundsProperty.getValue());
        m_AreTurnsOver.setValue(false);
        m_CurrentGameIndexProperty.set(0);
    }

    public void onMoveMade(){
        m_CurrentPlayer.OnMove(m_CurrentGame,m_TurnNumberProperty.getValue(), m_AreTurnsOver.getValue());
        m_RoomInfo.OnMove(m_Games, m_RoundNumberProperty.getValue(), m_CurrentPlayer.name);
    }

    public void DoMove(MoveInfo i_moveInfo){
        super.DoMove(i_moveInfo);
        onMoveMade();
    }

    public CurrentPlayerInfo getCurrentPlayer() {
        return m_CurrentPlayer;
    }

    public void HandleMove(MoveType i_MoveType) {
        switch (i_MoveType) {
            case Undo:
                super.UndoMove();
                break;
            case Redo:
                super.RedoMove();
                break;
            case Skip:
                super.MoveToNextGame();
                break;
        }

        onMoveMade();
    }

    public void DoComputerMove() {
//        boolean choseSkip = false;
        MoveType moveType = MoveType.getRandom(); // never chooses Skip
        switch (moveType) {
            case Normal:
                HandleComputerMove();
                break;
            case Undo:
                UndoMove();
                System.out.println("Undo");
                break;
            case Redo:
                RedoMove();
                System.out.println("Redo");
                break;
            case Skip:
//                choseSkip = true;
                System.out.println("Skip");
                break;
            default:
                break;
        }
//        return choseSkip;

        onMoveMade();
    }

    public CurrentPlayerInfo getPlayerInfo(String i_UserName) {
        CurrentPlayerInfo playerInfo = null;

        JavaFxSupGame game = getGameByUserName(i_UserName);

        if (game != null) {
            playerInfo = new CurrentPlayerInfo(game, this);
        }

        return playerInfo;
    }
}
