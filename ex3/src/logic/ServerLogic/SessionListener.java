package logic.ServerLogic;

import Utils.Constants;
import Utils.ServletUtils;
import Utils.SessionUtils;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by yakir on 10/27/16.
 *
 */

@WebListener
public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        removeUserFromGame(se);
        logoutUser(se);
    }

    private void removeUserFromGame(HttpSessionEvent se) {
        String username = SessionUtils.getAttr(se.getSession(), Constants.Username);
        String roomName = SessionUtils.getAttr(se.getSession(), Constants.RoomName);

        if (roomName != null) {
            RoomsManager roomManager = ServletUtils.getRoomsManager(se.getSession().getServletContext());
            roomManager.RemoveUserFromRoom(username, roomName, se.getSession());
        }
    }

    private void logoutUser(HttpSessionEvent se) {
        String username = SessionUtils.getAttr(se.getSession(), Constants.Username);
        if (username != null) {
            UsersManager userManager = ServletUtils.getUsersManager(se.getSession().getServletContext());
            userManager.Remove(username);
        }
    }
}
