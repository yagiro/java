package logic.ServerLogic;

import logic.ServerLogic.Models.UserModel;
import logic.SupGame.JavaFxSup.Main.Model.JavaFxSupGame;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hagai on 10/20/16.
 *
 */

public class RoomInfo extends ShortRoomInfo {
    private HashMap<String, PlayerInfo> players;
    private int version;
    private BoardInfo boardInfo;
    private Boolean hadStarted;
    private int round;
    private final int totalMoves = 2;
    private String currentPlayer;
    private GameOverInfo gameOver;
    private boolean isGameOver;

    public RoomInfo() {
        players = new HashMap<>();
        version = 1;
    }

    public RoomInfo(ShortRoomInfo i_ShortRoomInfo) {
        this();
        populate(i_ShortRoomInfo);
    }

    private void populate(ShortRoomInfo i_ShortRoomInfo) {
        name = i_ShortRoomInfo.name;
        admin = i_ShortRoomInfo.admin;
        rounds = i_ShortRoomInfo.rounds;
        boardSize = i_ShortRoomInfo.boardSize;
        playersCount = i_ShortRoomInfo.playersCount;
        ++version;
    }

    public RoomInfo(ShortRoomInfo i_ShortRoomInfo, BoardInfo i_BoardInfo){
        this(i_ShortRoomInfo);
        boardInfo = i_BoardInfo;
    }

    public HashMap<String, PlayerInfo> getPlayers() {
        return players;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void PlayerJoined(UserModel i_UserModel) {
        players.put(i_UserModel.getUsername(), new PlayerInfo(i_UserModel.getPlayerType()));
        playersCount.setAvailablePlayers(players.size());
        ++version;
    }

    public void PlayerLeft(String i_UserName) {
        players.remove(i_UserName);
        playersCount.setAvailablePlayers(players.size());
        ++version;
    }

    public BoardInfo getBoardInfo() {
        return boardInfo;
    }

    public void setBoardInfo(BoardInfo boardInfo) {
        this.boardInfo = boardInfo;
    }

    public Boolean getHadStarted() {
        return hadStarted;
    }

    public void setHadStarted(Boolean hadStarted) {
        this.hadStarted = hadStarted;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public void OnStartGame(ArrayList<JavaFxSupGame> i_Games, Integer i_Round, String i_CurrentPlayer) {
        hadStarted = true;
        refreshData(i_Games, i_Round, i_CurrentPlayer);
    }

    public void OnMove(ArrayList<JavaFxSupGame> i_Games, Integer i_Round, String i_CurrentPlayer) {
        refreshData(i_Games, i_Round, i_CurrentPlayer);
    }

    private void refreshData(ArrayList<JavaFxSupGame> i_Games, Integer i_Round, String i_CurrentPlayer) {
        round = i_Round;
        currentPlayer = i_CurrentPlayer;
        updatePlayers(i_Games);
        checkGameOver(i_Games);
        ++version;
    }

    private void checkGameOver(ArrayList<JavaFxSupGame> i_Games) {
        // check if rounds over
        if (round > rounds) {
            isGameOver = true;
            gameOver = new GameOverInfo(GameOverInfo.Reason.RoundsOver);
        }

        // check single player left
        if (i_Games.size() == 1 && playersCount.getRequiredPlayers() > 1) {
            isGameOver = true;
            gameOver = new GameOverInfo(GameOverInfo.Reason.SinglePlayerLeft, currentPlayer);
        }

        // check if win
        for(JavaFxSupGame game : i_Games){
            if (game.scoreProperty().getValue() == 100) {
                isGameOver = true;
                gameOver = new GameOverInfo(GameOverInfo.Reason.Win, game.getPlayer().getName());
                break;
            }
        }
    }

    private void updatePlayers(ArrayList<JavaFxSupGame> i_Games){
        for(JavaFxSupGame game : i_Games){
            PlayerInfo player = players.get(game.getPlayer().getName());
            player.setScore(game.scoreProperty().getValue());
        }
    }

    public void Reset() {
        hadStarted = false;
        version = 1;
        currentPlayer = "";
        gameOver = null;
        isGameOver = false;
    }
}
