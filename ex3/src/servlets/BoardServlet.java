package servlets;

import Responses.BoardResponse;
import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hagai on 25/10/2016.
 */
@WebServlet(name="BoardServlet", urlPatterns = { "/board" })
public class BoardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if(userIsLoggedIn(req)) {
            String roomName = req.getParameter(Constants.Name);
            BoardResponse boardResponse = ServletUtils.getBoardResponse(req, roomName);
            JsonUtils.sendJson(res, boardResponse);
        }
        else{
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
        }
    }

    private boolean userIsLoggedIn(HttpServletRequest req) {
        String username = SessionUtils.getAttr(req.getSession(), Constants.Username);
        return username != null;
    }
}
