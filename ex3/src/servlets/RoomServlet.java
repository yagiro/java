package servlets;

import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.ServerLogic.RoomInfo;
import logic.ServerLogic.RoomsManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/22/16.
 *
 */

@WebServlet(name = "RoomServlet", urlPatterns = { "/room" })
public class RoomServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String roomName = SessionUtils.getAttr(req, Constants.RoomName);

        if (roomName == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return;
        }

        RoomsManager roomsManager = ServletUtils.getWebManager(req).getRoomsManager();
        RoomInfo roomInfo = roomsManager.getRoom(roomName).getInfo();
        JsonUtils.sendJson(res, roomInfo);
    }
}
