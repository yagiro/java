package servlets;

import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.ServerLogic.CurrentPlayerInfo;
import logic.ServerLogic.RoomsManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hagai on 29/10/2016.
 *
 */

@WebServlet(name = "PlayerStateServlet", urlPatterns = { "/playerState" })
public class PlayerStateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String userName = SessionUtils.getAttr(req, Constants.Username);
        String roomName = SessionUtils.getAttr(req, Constants.RoomName);

        if (userName == null || roomName == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return;
        }

        CurrentPlayerInfo playerInfo = ServletUtils.getRoomsManager(req).getRoom(roomName).getPlayerInfo(userName);

        if (playerInfo != null) {
            JsonUtils.sendJson(res, StatusCodes.Ok, playerInfo);
        }
        else {
            JsonUtils.sendJson(res, StatusCodes.NotFound, userName + " is not in the room");
        }
    }
}
