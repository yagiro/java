package servlets;

import Responses.LobbyResponse;
import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/13/16.
 *
 */

@WebServlet(name = "LobbyServlet", urlPatterns = {"/lobby"})
public class LobbyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String username = SessionUtils.getAttr(req, Constants.Username);

        if (username == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return;
        }

        LobbyResponse lobbyRes = ServletUtils.getLobbyResponse(req, username);
        JsonUtils.sendJson(res, lobbyRes);
    }

}
