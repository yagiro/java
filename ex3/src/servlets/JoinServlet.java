package servlets;

import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.ServerLogic.Exceptions.RoomClosedException;
import logic.ServerLogic.Exceptions.UserAlreadyInRoomException;
import logic.ServerLogic.Models.UserModel;
import logic.ServerLogic.SupWebManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/20/16.
 *
 */

@WebServlet(name = "JoinServlet", urlPatterns = { "/join" })
public class JoinServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        String roomName = req.getParameter(Constants.RoomName);
        String userName = SessionUtils.getAttr(req, Constants.Username);

        if (userName == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return;
        }

        if (roomName.equals("undefined")) {
            JsonUtils.sendJson(res, StatusCodes.BadRequest);
            return;
        }

        SupWebManager webManager = ServletUtils.getWebManager(req);
        UserModel userModel = webManager.getUsersManager().getUserModel(userName);

        try {
            ServletUtils.getWebManager(req).getRoomsManager().JoinUserToRoom(userModel, roomName, req);
            JsonUtils.sendJson(res, StatusCodes.Ok, userModel);
        }
        catch (RoomClosedException rce) {
            JsonUtils.sendJson(res, StatusCodes.RoomClosed, userModel.getUsername());
        }
        catch (UserAlreadyInRoomException ue) {
            JsonUtils.sendJson(res, StatusCodes.BadRequest, "User " + userName + " is already logged in to the room.");
        }
    }
}
