package servlets;

import Responses.StatusCodes;
import Responses.UserStateResponse;
import Responses.UserStates;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/26/16.
 *
 */

@WebServlet(name="UserStateServlet", urlPatterns = { "/userState" })
public class UserStateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String userName, roomName, userType;

        userName = SessionUtils.getAttr(req, Constants.Username);
        roomName = SessionUtils.getAttr(req, Constants.RoomName);
        userType = SessionUtils.getAttr(req, Constants.PlayerType);

        if (userName == null) {
            JsonUtils.sendJson(res, StatusCodes.Ok,
                    new UserStateResponse(UserStates.None, "/login.html"));
        } else if (roomName == null) {
            JsonUtils.sendJson(res, StatusCodes.Ok,
                    new UserStateResponse(UserStates.LoggedIn, "/lobby.html"));
        } else {
            JsonUtils.sendJson(res, StatusCodes.Ok,
                    new UserStateResponse(UserStates.InRoom, getRoomUrl(userName, roomName, userType), roomName));
        }
    }

    private String getRoomUrl(String userName, String roomName, String userType) {
        return String.format("/room.html?name=%s&username=%s&userType=%s", roomName, userName, userType);
    }
}
