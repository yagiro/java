package servlets;

import Utils.Constants;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.ServerLogic.RoomsManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/22/16.
 *
 */

@WebServlet(name="LeaveServlet", urlPatterns = { "/leave" })
public class LeaveServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = SessionUtils.getAttr(req.getSession(), Constants.Username);

        if (username != null) {
            String roomName = SessionUtils.getAttr(req, Constants.RoomName);

            if (roomName != null) {
                RoomsManager roomsManager = ServletUtils.getWebManager(req).getRoomsManager();
                roomsManager.RemoveUserFromRoom(username, roomName, req);
            }
        }

        resp.sendRedirect("lobby.html");
    }
}
