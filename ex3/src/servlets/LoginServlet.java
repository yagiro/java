package servlets;

import Responses.StatusCodes;
import Utils.*;
import logic.GameInfrastructure.PlayerType;
import logic.ServerLogic.Models.UserModel;
import logic.ServerLogic.UsersManager;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/6/16.
 *
 */

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html;charset=UTF-8");

        String userNameFromSession = SessionUtils.getAttr(req.getSession(), Constants.Username);

        if (userNameFromSession == null) {

            String userNameFromRequest = req.getParameter(Constants.Username);
            String playerTypeFromRequest = req.getParameter(Constants.PlayerType);

            if (userNameFromRequest == null || playerTypeFromRequest == null) {
                JsonUtils.sendJson(res, StatusCodes.BadRequest, "Missing at least one parameter (username, player type).");
            }
            else {
                // all good. new user trying to login.
                PlayerType playerType = null;
                try {
                    playerType = PlayerType.Parse(playerTypeFromRequest);
                    UserModel newUserModel = new UserModel(userNameFromRequest, playerType);
                    handleNewUser(req, res, newUserModel);
                }
                catch (GameDescriptorException gde) {
                    JsonUtils.sendJson(res, StatusCodes.BadRequest, "Player type is not valid.");
                }
            }
        }
        else {
            // user with open session - redirect to game room
            JsonUtils.sendJson(res, StatusCodes.BadRequest, "User is already logged in.");
        }
    }

    private void handleNewUser(HttpServletRequest req, HttpServletResponse res, UserModel i_UserModel) throws IOException {
        // new user trying to log in: save session > save in logic > redirect to game room
        UsersManager usersManager = ServletUtils.getUsersManager(getServletContext());

        if (i_UserModel.getUsername().matches("^\\s*$")) {
            JsonUtils.sendJson(res, StatusCodes.BadRequest, "Username must have at least one character.");
            return;
        }

        if (!usersManager.Contains(i_UserModel)) {
            // All good. new session, username is not taken.
            SessionUtils.RegisterLogin(req, i_UserModel);
            usersManager.Add(i_UserModel);
            JsonUtils.sendJson(res, StatusCodes.Created);
        }
        else {
            JsonUtils.sendJson(res, StatusCodes.UserNameTaken);
        }
    }
}