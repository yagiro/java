package servlets;

import Utils.Constants;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.ServerLogic.RoomsManager;
import logic.ServerLogic.UsersManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yakir on 10/19/16.
 *
 */

@WebServlet(name="LogoutServlet", urlPatterns={"/logout"})
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String username = SessionUtils.getAttr(req.getSession(), Constants.Username);

        if (username != null) {
            UsersManager usersManager = ServletUtils.getUsersManager(req.getServletContext());
            usersManager.Remove(username);
            SessionUtils.RegisterLogout(req.getSession());
        }

        res.sendRedirect("login.html");
    }
}
