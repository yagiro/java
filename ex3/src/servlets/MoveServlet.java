package servlets;

import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import com.google.gson.Gson;
import logic.GameInfrastructure.PlayerType;
import logic.ServerLogic.Models.MoveRequest;
import logic.ServerLogic.Room;
import logic.SupGame.Logic.MoveInfo;
import logic.SupGame.Logic.MoveType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by yakir on 10/27/16.
 *
 */

@WebServlet(name = "MoveServlet", urlPatterns = { "/move" })
public class MoveServlet extends HttpServlet {
    String userName;
    String roomName;
    PlayerType playerType;
    Room room;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (populateAndValidate(req, res)) {
            Map<String, String[]> paramsMap = req.getParameterMap();
            MoveInfo move;
            try {
                move = ServletUtils.ExtractMoveInfoFromRequest(paramsMap);
            } catch (Exception e) {
                JsonUtils.sendJson(res, StatusCodes.BadRequest, e);
                return;
            }

            Room room = ServletUtils.getRoomsManager(req).getRoom(roomName);
            try {
                room.DoMove(move);
                JsonUtils.sendJson(res, StatusCodes.Ok, room.getCurrentPlayer());
            } catch (Exception e) {
                JsonUtils.sendJson(res, StatusCodes.BadRequest, e);
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (populateAndValidate(req, res)) { // get user and room and player type

            MoveType moveType = null;

            try {
                moveType = MoveType.Parse(req.getParameter(Constants.Type));
            } catch (Exception e) {
                if (playerType == PlayerType.Human) {
                    JsonUtils.sendJson(res, StatusCodes.BadRequest, e);
                    return;
                }
            }

            try {
                switch (playerType) {
                    case Human:
                        room.HandleMove(moveType);
                        break;
                    case Computer:
                        if (moveType != MoveType.Skip) {
                            room.DoComputerMove();
                        }
                        else {
                            room.HandleMove(moveType);
                        }
                        break;
                    default:
                        // playerType already validated
                        break;
                }
            }
            catch (Exception e) {
                JsonUtils.sendJson(res, StatusCodes.BadRequest, e);
                return;
            }

            JsonUtils.sendJson(res, StatusCodes.Ok, room.getCurrentPlayer());
        }
    }

    private boolean populateAndValidate(HttpServletRequest req, HttpServletResponse res) throws IOException{
        userName = SessionUtils.getAttr(req, Constants.Username);
        roomName = SessionUtils.getAttr(req, Constants.RoomName);

        try {
            playerType = PlayerType.Parse(SessionUtils.getAttr(req, Constants.PlayerType));
        }
        catch(Exception e) {
            JsonUtils.sendJson(res, StatusCodes.BadRequest, "invalid player type value.");
            return false;
        }

        if (userName == null || roomName == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return false;
        }

        room = ServletUtils.getRoomsManager(req).getRoom(roomName);

        if (!userName.equals(room.getCurrentPlayer().name)) {
            JsonUtils.sendJson(res, StatusCodes.BadRequest, "Only current player can make a move.");
            return false;
        }

        return true;
    }
}
