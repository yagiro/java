package servlets;

import Responses.StatusCodes;
import Utils.Constants;
import Utils.JsonUtils;
import Utils.ServletUtils;
import Utils.SessionUtils;
import logic.Generated.GameDescriptor;
import logic.ServerLogic.Exceptions.RoomNameTakenException;
import logic.ServerLogic.ShortRoomInfo;
import logic.ServerLogic.Models.UserModel;
import logic.ServerLogic.SupWebManager;
import logic.SupGame.Logic.Exceptions.GameDescriptorException;
import logic.SupGame.Utils.XmlUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

/**
 * Created by yakir on 10/19/16.
 *
 */

@WebServlet(name="CreateRoomServlet", urlPatterns = { "/create" })
public class CreateRoomServlet extends HttpServlet {
    private SupWebManager webManager;
    private GameDescriptor gameDescriptor;
    private String username;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (isValid(req, res)) { // isValid() populates the members as well

            try {
                webManager.getRoomsManager().CreateRoom(gameDescriptor, username);
                JsonUtils.sendJson(res, gameDescriptor.getDynamicMultiPlayers().getGametitle());
            }
            catch (GameDescriptorException gde) {
                JsonUtils.sendJson(res, StatusCodes.InvalidGameDescriptor, gde.getMessage());

            }
            catch (RoomNameTakenException gnte) {
                JsonUtils.sendJson(res, StatusCodes.RoomNameTaken, gameDescriptor.getDynamicMultiPlayers().getGametitle());
            }
        }
    }

    private boolean isValid(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        username = SessionUtils.getAttr(req.getSession(), Constants.Username);
        webManager = ServletUtils.getWebManager(req.getServletContext());

        if (username == null) {
            JsonUtils.sendJson(res, StatusCodes.Unauthorized);
            return false;
        }

        if (webManager == null) {
            JsonUtils.sendJson(res, StatusCodes.InternalServerError, "WebManager is null.");
            return false;
        }

        gameDescriptor = null;
        try {
            extractDataFromGameFile(req);
        } catch (GameDescriptorException gde) {
            JsonUtils.sendJson(res, StatusCodes.InvalidGameDescriptor, gde.getMessage());
            return false;
        }
        catch (Exception e) {
            // probably file exception.
            JsonUtils.sendJson(res, StatusCodes.FileError, e.getMessage());
            return false;
        }

        return true;
    }

    private void extractDataFromGameFile(HttpServletRequest req) throws Exception {
        InputStream gameDescriptorStream = ServletUtils.GetInputStreamFromFileInTheRequest(req);
        gameDescriptor = XmlUtils.ParseGameDescriptorFromXml(gameDescriptorStream);
    }
}
