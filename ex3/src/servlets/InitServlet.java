package servlets;

import Utils.Constants;
import Utils.ServletUtils;
import Utils.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hagai on 15/10/2016.
 *
 */

@WebServlet(name = "InitServlet", urlPatterns = {"/init"})
public class InitServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String usernameSession = SessionUtils.getAttr(req.getSession(), Constants.Username);
        if(usernameSession != null){
            res.sendRedirect("lobby.html");
        }
        else{
            res.sendRedirect("login.html");
        }
    }
}
