package Responses;

/**
 * Created by yakir on 10/26/16.
 *
 */

public class UserStateResponse {
    public int userState;
    public String redirectTo;
    public String roomName;

    public UserStateResponse(int userState) {
        this.userState = userState;
    }

    public UserStateResponse(int userState, String redirectTo) {
        this(userState);
        this.redirectTo = redirectTo;
    }

    public UserStateResponse(int userState, String redirectTo, String roomName) {
        this(userState, redirectTo);
        this.roomName = roomName;
    }
}
