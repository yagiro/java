package Responses;

import com.sun.javafx.geom.Vec2d;
import logic.ServerLogic.BoardInfo;
import logic.SupGame.Logic.SupBoard;

import java.awt.color.ICC_ColorSpace;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Hagai on 26/10/2016.
 */
public class BoardResponse {
    private ArrayList<ArrayList<Integer>> rowStreaks, colStreaks;
    private Vec2d boardSize;

    public BoardResponse(){
        boardSize = new Vec2d();
    }
    public BoardResponse(BoardInfo i_BoardInfo, Vec2d i_BoardSize){
        this();
        ArrayList<SupBoard.Streak> boardRowStreaks = i_BoardInfo.getRowStreaks();
        ArrayList<SupBoard.Streak> boardColStreaks = i_BoardInfo.getColStreaks();
        boardSize = i_BoardSize;
        rowStreaks = BoardResponse.getStreaks(boardRowStreaks);
        colStreaks = BoardResponse.getStreaks(boardColStreaks);
    }

//    public BoardResponse(ArrayList<SupBoard.Streak> i_RowStreaks, ArrayList<SupBoard.Streak> i_ColStreaks, Vec2d i_BoardSize){
//        boardSize = i_BoardSize;
//        rowStreaks = BoardResponse.getStreaks(i_RowStreaks);
//        colStreaks = BoardResponse.getStreaks(i_ColStreaks);
//    }

    private static ArrayList<ArrayList<Integer>> getStreaks(ArrayList<SupBoard.Streak> i_Streaks){
        ArrayList<ArrayList<Integer>> res = new ArrayList();
        int index = 0;
        for(SupBoard.Streak streak : i_Streaks){
            res.add(new ArrayList<>());
            for(SupBoard.SingleStreak singleStreak : streak.getStreakLengths()){
                res.get(index).add(singleStreak.getValue());
            }
            ++index;
        }

        return res;
    }


}
