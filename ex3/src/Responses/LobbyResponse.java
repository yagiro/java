package Responses;

import logic.ServerLogic.ShortRoomInfo;
import logic.ServerLogic.Models.UserModel;
import logic.ServerLogic.Models.Versions;
import logic.ServerLogic.SupWebManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by yakir on 10/13/16.
 *
 */

public class LobbyResponse {
    private ArrayList<UserModel> users;
    private HashMap<String, ShortRoomInfo> games;
    private Versions versions;
    private String username;

    public LobbyResponse() {
        users = new ArrayList<>();
        games = new HashMap<>();
        versions = new Versions();
    }

    public LobbyResponse(ArrayList<UserModel> i_Users, HashMap<String, ShortRoomInfo> i_Rooms, String i_UserName) {
        this();
        users = i_Users;
        games = i_Rooms;
        versions.setUsers(SupWebManager.getInstance().getUsersManager().getVersion());
        versions.setGames(SupWebManager.getInstance().getRoomsManager().getVersion());
        username = i_UserName;
    }
}
