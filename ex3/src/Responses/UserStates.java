package Responses;

/**
 * Created by yakir on 10/26/16.
 *
 */

public class UserStates {
    public static final int None = 0;
    public static final int LoggedIn = 1;
    public static final int InRoom = 2;
}
