package Responses;

/**
 * Created by hagai on 10/20/16.
 *
 */

public class StatusCodes {
    public static final int Ok = 200;
    public static final int Created = 201;
    public static final int BadRequest = 400;
    public static final int Unauthorized = 401;
    public static final int UserNameTaken = 403;
    public static final int NotFound = 404;
    public static final int RoomClosed = 501;
    public static final int InvalidGameDescriptor = 402;
    public static final int RoomNameTaken = 403;
    public static final int InternalServerError = 500;
    public static final int FileError = 502;

}
