var userStates = {
    none: 0,
    loggedIn: 1,
    inRoom: 2
};

var api = "http://localhost:8080/ex3";

function getQueryParam(name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getUserState(onSuccess, onError) {
    $.ajax({
        method: 'GET',
        url: api + '/userState',
        success: onSuccess,
        error: function(err) {
            console.log("Error: /userState", err);
            onError(err);
        }
    });
}