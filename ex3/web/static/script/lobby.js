var versions = {
    games: 0,
    users: 0
};

var selectedRoom,
    refreshInterval,
    parsers = {
        boardSize: function(boardSize) {
            return boardSize.x + "x" + boardSize.y;
        },
        playersCount: function(playersCount) {
            return playersCount.availablePlayers + "/" + playersCount.requiredPlayers;
        }
    },
    statusCodes = {
        badRequest: 400,
        userNotLoggedIn: 401,
        invalidGameDescriptor: 402,
        roomNameTaken: 403,
        roomClosed: 501
    },
    hideMessageTimeout,
    firstPopulate = true;

$(function(){
    getUserStateAndRedirectIfNeeded();
    populateTables();
    refreshInterval = setInterval(populateTables, 2000);
});

function getUserStateAndRedirectIfNeeded() {
    getUserState(function(res){
        if (res.userState != userStates.loggedIn) {
            location.href = api + res.redirectTo;
        }
    });
}

function populateTables(){

    $.ajax({
        url: api + "/lobby",
        method: "GET",
        success: function(res){
            if (firstPopulate) {
                firstPopulate = false;
                $("#username-title").html(res.username);
            }

            if (res.versions.users > versions.users) populateTable(res.users, "#users-body");
            if (res.versions.games > versions.games) {
                selectedRoom = null;
                populateTable(res.games, "#games-body");
                addPeekButtons();
            }
            $('#games-body > tr').css('cursor','pointer');
            versions.users = res.versions.users;
            versions.games = res.versions.games;
            setGamesTableClickEvent();
        },
        error: function(xhr,status,err){
            if (xhr.status == statusCodes.userNotLoggedIn) {
                askToLogin();
            }
            disablePage();
            window.clearInterval(refreshInterval);
            console.log("failed to enter lobby", err);
        }
    });
}

function addPeekButtons() {
    $("#games-body > tr").each(function(){
        $(this).append("<td><button id='"+ $(this).attr('id') +"' class='btn active' onclick='onPeek(this)'>Peek</button></td>");
    });
}

function onPeek(peekBtn) {
    var roomName = $(peekBtn).attr('id');

    $.ajax({
        method: 'GET',
        url: api + '/board?name=' + roomName,
        success: function(boardResponse){
            showBoard(boardResponse);
        },
        error: function(xhr, status, err) {
            console.log('error fetching board info', xhr);
        }
    });
}

function showBoard(boardResponse) {
    buildBoard(boardResponse);
    $(".peek-div").css('visibility', 'visible');
}

function buildBoard(boardResponse){
    $board = $("#board");
    $board.empty();

    var id;
    var boardRows = boardResponse.boardSize.x;
    var boardCols = boardResponse.boardSize.y;
    var streak;

    // adjustBoardSize(boardResponse.boardSize.y);
    buildColStreaks(boardResponse.colStreaks);

    for(var row = 0; row < boardRows ; row++) {
        streak = buildRowStreak(boardResponse.rowStreaks[row]);
        $board.append("<tr id=\""+row+"\">");
        $("tr#"+row).append("<td id = \"rs-" + row + "\" class='streak'>"+streak+"</td>");
        for(var col = 0; col < boardCols ; col++) {
            id = row+"-"+col;
            $("tr#"+row).append("<td id=\""+id+"\" class = \"cell Undefined\"></td>");
        }
        $board.append("</tr>");
    }

    // $("div.board-container").height($("div.board-container").width());
    var cellWidth = $("td.cell").width();
    $("td.cell").height(cellWidth);
}

function buildRowStreak(streak) {
    var par = "<span>";
    for(var length in streak){
        par += streak[length] + " ";
    }
    par+="</span>";
    return par;
}

function buildColStreaks(colStreaks){
    var index = 0;
    $("#board").append("<tr id=\"colStreaks\">");
    $("tr#colStreaks").append("<td id=\"dummy\" class=\"streak\"></td>");
    for(var streak in colStreaks){
        buildColStreak(colStreaks[streak], index);
        ++index;
    }
    $("#board").append("</tr>");
}

function buildColStreak(streak, index){
    var colStreak = "";
    for(var length in streak){
        colStreak+= "<span>" +  streak[length] + "</span><br>";
    }
    $("tr#colStreaks").append("<td id=\"cs-" + index +"\">"+colStreak+"</td>");
}

function closePeek() {
    $(".peek-div")
        .css('visibility', 'hidden');
}

function logout() {
    location.href = api + "/logout";
}

function askToLogin() {
    $('#message').html("<div id=\"message-text\">Please <a href=\"http://localhost:8080/ex3/login.html\">login</a> first.</div>").css('visibility', 'visible');
}

function disablePage() {
    $(".login-only").css('filter', 'blur(1px)');
    $("button.login-only")
        .removeAttr('onclick')
        .prop("disabled", 'disabled')
        .removeClass('active');
}

function setGamesTableClickEvent() {
    $("#games-body > tr").on('click', function() {
        onGameClicked(this);
    });
}

function onGameClicked(gameRow) {
    var gameName = gameRow.id;
    selectedRoom = gameName;
    console.log('room click', selectedRoom);
    markGameRow(gameRow);
}

function markGameRow(gameRow) {
    $("#games-body > tr").css('background-color', '');
    $(gameRow).css('background-color', 'pink');
}

function populateTable(data, tableId){
    $(tableId).html("");

    for(var key in data) {
        $(tableId).append(buildTableRow(data[key]));
    }
    console.log("populated " + tableId, data);
}

function buildTableRow(item){
    var tr = $("<tr></tr>");

    if (item.name) {
        tr.attr('id', item.name);
    }

    for(var key in item) {
        var field = item[key];
        if (parsers[key]) {
            field = parsers[key](item[key]);
        }
        tr.append("<td>"+field+"</td>")
    }

    return tr;
}

function joinGameClick() {
    if (selectedRoom) {
        $.ajax({
            method: 'GET',
            url: api + '/join?roomName=' + selectedRoom,
            success: function(userModel) {
                location.href = api + '/room.html?name=' + selectedRoom + '&username=' + userModel.username + "&userType=" + userModel.playerType
            },
            error: function(xhr) {
                var err = xhr.responseText;
                switch (xhr.status) {
                    case statusCodes.roomClosed:
                        informUser('Room ' + selectedRoom + ' is closed.', 'red');
                        console.log('Room ' + selectedRoom + ' is closed.', xhr);
                        break;
                    case statusCodes.badRequest:
                        informUser(err, 'red');
                        console.log('Error: Join to ' + selectedRoom, err);
                        break;
                    default:
                        console.log('Error: Join ' + selectedRoom, xhr);
                        break;
                }
            }
        });
    }
    else {
        informUser("Please pick a game first.", 'red');
    }
}

function informUser(message, color, time) {
    $(".controls-message")
        .html(message)
        .css('color', color)
        .css('opacity', '100');

    window.clearTimeout(hideMessageTimeout);
    hideMessageTimeout = setTimeout(hideControlsMessage, time || 4000);
}

function hideControlsMessage() {
    $(".controls-message").css('opacity', '0');
}

function toggleUploadForm() {
    $('#upload-div').fadeToggle('slow', 'linear');
}

function uploadGame() {
    if ($('#game-file').val() == '') {
        return informUser('Please select a file first.', 'orange');
    }
    if (!isValidFileType())
    {
        return informUser('You can only upload an xml file type.', 'red');
    }
    var formDataWithGameFile = buildFormDataWithGameFile();

    $.ajax({
        method: 'POST',
        url: api + '/create',
        data: formDataWithGameFile,
        cache: false,
        contentType: false,
        processData: false,
        success: onUploadSuccess,
        error: onUploadFailed
    });
}

function isValidFileType() {
    var type = $('#game-file').val().split('.').pop().toLowerCase();
    return type == 'xml';
}

function buildFormDataWithGameFile() {
    var formData = new FormData();
    var file = $("#game-file")[0].files[0];
    formData.append('file', file);
    return formData;
}

function onUploadSuccess(createdRoomName) {
    informUser("Room " + createdRoomName + " created!", 'green');
    toggleUploadForm();
    populateTables();
}

function onUploadFailed(xhr) {
    var err = xhr.responseText;
    switch (xhr.status) {
        case statusCodes.roomNameTaken:
            var roomName = err;
            informUser("The name <b>'" + roomName + "'</b> is already taken.<br>Please pick a different name.", 'orange');
            break;
        case statusCodes.invalidGameDescriptor:
            informUser("The game descriptor is not valid.\n" + err, 'red', 10000);
            break;
        default:
            informUser("Room was not created!", 'red');
            break;
    }
    console.log('Error: Upload', xhr);
}
