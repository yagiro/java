var errors = {
        formNotFilled: "formNotFilled",
        userExists: "userExists",
        serverError: "serverError"
    },
    statusCodes = {
        userNameTaken: 403
    };

$(function() {
    getUserStateAndRedirectIfNeeded();
});

function getUserStateAndRedirectIfNeeded() {
    getUserState(function(res) {
        if (res.userState != userStates.none) {
            location.href = api + res.redirectTo;
        }
    });
}

function login() {
    var $playerTypeCheckedButton = $("input[name=playerType]:checked");
    var username = $("#username").val().trim();
    var playerType = $playerTypeCheckedButton.val();
    var usernameIsValid = /\S/.test(username); // contains at least one character (aka: is not only whitespace).
    var formIsFilled = usernameIsValid && $playerTypeCheckedButton.length > 0;

    if (!formIsFilled) {
        return showError(errors.formNotFilled);
    }

    $.ajax({
        method: 'GET',
        url: api + "/login?username=" + username + "&playerType=" + playerType,
        success: onLoginSuccess,
        error: onLoginFailed
    });
}

function onLoginSuccess() {
    location.href = api + "/lobby.html";
}

function onLoginFailed(xhr) {
    switch(xhr.status) {
        case statusCodes.userNameTaken:
            showError(errors.userExists);
            break;
        default:
            showError(errors.serverError);
            break;
    }
}

function showError(err) {
    var message = "";

    switch(err) {
        case "formNotFilled":
            message = "Please make sure to enter your username and choose a player type.";
            break;
        case "userExists":
            message = "That user name already exists.";
            break;
        default:
            message = "Oops. Something went wrong. Please try again.";
            break;
    }

    $("#error-container")
        .html(message)
        .css('visibility', 'visible');

}
