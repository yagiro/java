var roomName,
    username, userType,
    api = "http://localhost:8080/ex3",
    version = 0,
    refreshInterval,
    isActive = false,
    isMyTurn = false,
    selectedCells = [],
    Coord = function(x,y){
        this.x = x;
        this.y = y;
    },
    boardSize = {
        'small': 15,
        'medium': 20,
        'large': 30,
        'xl': 35
    },
    hideMessageTimeout,
    requiredPlayers,
    totalMoves,
    isTurnsOver,
    firstRefresh = true,
    wasWaitingForGameToStart,
    currMove = 1,
    reasons = {
        win: "Win",
        roundsOver: "RoundsOver",
        singlePlayerLeft: "SinglePlayerLeft"
    };

$(function(){
    init();
});

function init() {
    getDataFromQueryParams();
    getUserStateAndRedirectIfNeeded();
    $("#title").html(roomName);
    $("#username-title").html(username);
    buildBoard();
    initPlayerState();
    refreshPage();
    refreshInterval = setInterval(refreshPage, 2000);

    if (userType == 'Computer') {
        disableControls();
    }
}

function initPlayerState() {
    $.ajax({
        method: 'GET',
        url: api + '/room',
        success: function(roomInfo) {
            totalMoves = roomInfo.totalMoves;
            isMyTurn = username == roomInfo.currentPlayer;
            getPlayerState();
        }
    });
}

function getPlayerState() {
    $.ajax({
        method: 'GET',
        url: api + '/playerState',
        success: onPlayerInfoSuccess,
        error: function (err) {
            console.log('Error: player state', err);
        }
    });
}

function onPlayerInfoSuccess(playerInfo) {
    if (isMyTurn) {
        isTurnsOver = playerInfo.isTurnsOver;
    }
    else {
        isTurnsOver = false;
    }
    // clearSelectedCells();
    currMove = Math.min(totalMoves, playerInfo.move);
    adjustControls(playerInfo);
    // if(isMyTurn) {
        populatePlayerInfo(playerInfo);
        updateBoard(playerInfo.boardInfo);
    // }
}

function getDataFromQueryParams() {
    roomName = getQueryParam('name');
    username = getQueryParam('username');
    userType = getQueryParam('userType');
}

function getUserStateAndRedirectIfNeeded() {
    getUserState(function(res){
        if (res.userState != userStates.inRoom || res.roomName != roomName) {
            location.href = api + res.redirectTo;
        }
    });
}

function refreshPage() {
    $.ajax({
        method: 'GET',
        url: api + '/room',
        success: onRefreshSuccess,
        error: function(xhr, status, err) {
            console.log('Error: refresh page',xhr);
            window.clearInterval(refreshInterval);
        }
    });
}

function onRefreshSuccess(roomInfo) {
    if (roomInfo.version > version) {
        if (firstRefresh) {
            firstRefresh = false;
            if (!roomInfo.hadStarted) {
                wasWaitingForGameToStart = true;
                $("#wait-msg").fadeIn(700);
            }
        }

        isMyTurn = roomInfo.currentPlayer == username;

        if (roomInfo.hadStarted && !isActive) {
            beginGame();
        }
        populatePage(roomInfo);
        populateRoomInfo(roomInfo);
        requiredPlayers = roomInfo.playersCount.requiredPlayers;
        totalMoves = roomInfo.totalMoves;

        if (isMyTurn && userType == "Computer") {
            doComputerRound();
        }

        if (roomInfo.isGameOver) {
            onGameOver(roomInfo.gameOver);
        }

        var $leaveBtn = $(".btn.leave");

        if (isActive) {
            if (isMyTurn) {
                if (userType == 'Human') {
                    $leaveBtn.prop("disabled", "");
                }
            }
            else {
                $leaveBtn.prop("disabled", "disabled");
            }
        }

        version = roomInfo.version;
        console.log('page refreshed', roomInfo);
    }
}

function onGameOver(gameOverInfo) {
    isActive = false;
    $(".btn.leave").prop("disabled", "");
    var message = "";
    var subtitle = "";
    disablePage();
    window.clearInterval(refreshInterval);

    switch (gameOverInfo.reason) {
        case reasons.win:
            var winner = gameOverInfo.winner == username ? "You" : gameOverInfo.winner;
            message = winner +  " won!";
            break;
        case reasons.roundsOver:
            message = "Game Over";
            subtitle = "No more rounds.";
            break;
        case reasons.singlePlayerLeft:
            message = "You won!";
            subtitle = "Everyone left.";
            break;
        default:
            message = "Game Over";
            break;
    }

    showGameOver(message, subtitle);
}

function disablePage() {
    disableControls();
    $(".cell").unbind("click");
}

function disableControls() {
    $(".control").prop("disabled", "disabled");
}

function showGameOver(message, subtitle) {
    $okBtn = $("<button class='btn active' onclick='onGameOverOkClick()'>OK</button>")
    $content = $("<div id='game-over-message-text'></div>");
    $content
        .append(message + "<br>")
        .append("<span id='game-over-subtitle'>"+ subtitle +"</span><br><br>")
        .append($okBtn);

    $("#game-over-message")
        .html($content)
        .css("visibility", "visible");
}

function onGameOverOkClick() {
    $("#game-over-message").css("visibility", "hidden");
}

function doComputerRound() {
    doComputerMove();
}

function doComputerMove() {
    $.ajax({
        method: 'GET',
        url: api + '/move',
        success: function(playerInfo) {
            onMoveSuccess(playerInfo);
            if (!playerInfo.isTurnsOver) {
                doComputerMove();
            }
            else {
                onNextTurn();
            }
        }
    });
}

function populateRoomInfo(roomInfo) {
    $roomInfo = $("#room-info");
    $roomInfo
        .html("")
        .append("Round: " + Math.min(roomInfo.round, roomInfo.rounds) + "/" + roomInfo.rounds + "<br>")
        .append("Score: <span id='player-score'>" + roomInfo.players[username].score + "</span>");

    if (isMyTurn) {
        $roomInfo.append("<br>" + "Move: <span id='player-move'>" + currMove + "</span>/" + roomInfo.totalMoves);
    }
}

function onCellClick(cell){
    if($(cell).hasClass("selected")){
        unSelectCell(cell);
    }
    else{
        selectCell(cell);
    }
}

function selectCell(cell){
    $(cell).addClass("selected");
    addSelectedCell(cell);

}

function unSelectCell(cell){
    $(cell).removeClass("selected");
    removeSelectedCell(cell);
}

function addSelectedCell(cell){
    var id = $(cell).attr('id');
    var x = id.split('-')[0];
    var y = id.split('-')[1];
    selectedCells.push(new Coord(x,y));
}

function removeSelectedCell(cell){
    var id = $(cell).attr('id');
    var x = id.split('-')[0];
    var y = id.split('-')[1];
    var index = getIndexOfCell(x,y);
    selectedCells.splice(index,1);
}

function getIndexOfCell(x,y){
    for(var cell in selectedCells){
        if(selectedCells[cell].x == x && selectedCells[cell].y == y){
            return cell;
        }
    }
}

function beginGame(){
    isActive = true;
    $("td.cell").on('click', function(){
        onCellClick(this);
    });
    if (!isMyTurn) { disableControls(); }
    $("div#make-move-panel").fadeIn(700);
    $("#room-info").fadeIn(700);

    if (wasWaitingForGameToStart) {
        showGameStartMessage();
    }
    // else {
    //     $("#wait-msg").css('display', 'none');
    // }
}

function showGameStartMessage() {
    $("#wait-msg").html("Game started! Goodluck!");
    setTimeout(function(){
        $("#wait-msg").fadeOut(700);
    }, 4000);
}

function populatePage(roomInfo) {
    populatePlayers(roomInfo.players, roomInfo.currentPlayer);
    if (isMyTurn && userType == 'Human') {
        enableControls();
    }
    //initBoard(roomInfo.boardSize.x, roomInfo.boardSize.y);
}

function enableControls() {
    setControlsDisabledProperty("");

    if (isTurnsOver) {
        $("#move-button").prop("disabled", "disabled");
    }
}

function buildRowStreak(streak) {
    var par = "<span>";
    for(var length in streak){
        par += streak[length] + " ";
    }
    par+="</span>";
    return par;
}
function initBoard(boardResponse){
    $("#board").empty();
    var id;
    var boardRows = boardResponse.boardSize.x;
    var boardCols = boardResponse.boardSize.y;
    var streak;

    adjustBoardSize(boardResponse.boardSize.y);
    buildColStreaks(boardResponse.colStreaks);

    for(var row = 0; row < boardRows ; row++) {
        streak = buildRowStreak(boardResponse.rowStreaks[row]);
        $("#board").append("<tr id=\""+row+"\">");
        $("tr#"+row).append("<td id = \"rs-" + row + "\" class='streak'>"+streak+"</td>");
        for(var col = 0; col < boardCols ; col++) {
            id = row+"-"+col;
            $("tr#"+row).append("<td id=\""+id+"\" class = \"cell Undefined\"></td>");
        }
        $("#board").append("</tr>");
    }

    $("div.board-container").height($("div.board-container").width());
    var cellWidth = $("td.cell").width();
    $("td.cell").height(cellWidth);
}

function updateBoard(boardInfo) {
    updateStreaks(boardInfo.rowStreaks, "row");
    updateStreaks(boardInfo.colStreaks, "col");
    for (var i = 0; i < boardInfo.boardState.length; ++i) {
        for (var j = 0; j < boardInfo.boardState[i].length; ++j) {
            $cell = $("#" + i + "-" + j);
            $cell.removeClass("Blank Filled Undefined");
            $cell.addClass(boardInfo.boardState[i][j]);
        }
    }
}

function updateStreaks(streaks, type) {
    for (var i = 0; i < streaks.length; ++i ) {
        var t = type == "row" ? "r" : "c";
        var $streak = $("#" + t + "s-" + i).html("");
        var sep = type == "row" ? " " : "";
        for (var j = 0; j < streaks[i].m_StreakLengths.length; ++j) {
            var block = streaks[i].m_StreakLengths[j];
            var $block = $("<span>" + block.m_Value + sep + "</span>");

            if (block.m_IsPerfect) {
                $block.addClass("perfect");
            }
            $streak.append($block);

            if (type == "col") {
                $streak.append("<br>");
            }
        }
    }
}

function adjustBoardSize(colSize){

    /*
     'small': 15,
     'medium': 20,
     'large': 30,
     'xl': 35
     */
    if(colSize <= boardSize['small']){
        $("div#board-container").addClass('col-md-5')
    }
    else if(colSize <= boardSize['medium']){
        $("div#board-container").addClass('col-md-7')
    }
    else if(colSize <= boardSize['large']){
        $("div#board-container").addClass('col-md-9')
    }
    else if(colSize <= boardSize['xl']){
        $("div#board-container").addClass('col-md-12')
    }
    else{
        $("div#board-container").addClass('col-md-12')
    }
}

function buildColStreaks(colStreaks){
    var index = 0;
    $("#board").append("<tr id=\"colStreaks\">");
    $("tr#colStreaks").append("<td id=\"dummy\" class=\"streak\"></td>");
    for(var streak in colStreaks){
        buildColStreak(colStreaks[streak], index);
        ++index;
    }
    $("#board").append("</tr>");
}

function buildColStreak(streak, index){
    var colStreak = "";
    for(var length in streak){
        colStreak+= "<span>" +  streak[length] + "</span><br>";
    }
    $("tr#colStreaks").append("<td id=\"cs-" + index +"\">"+colStreak+"</td>");
}

function buildBoard(){
    $.ajax({
        method: 'GET',
        url: api + '/board?name=' + roomName,
        success: function(boardResponse){
            initBoard(boardResponse);

        },
        error: function(xhr, status, err) {
            console.log('error fetching board info', xhr);
            window.clearInterval(refreshInterval);
        }
    });
}

function initializeCell(cell,row,col){
    $(cell).addClass('undefined');
    $(cell).addClass('cell');
    $(cell).attr('row', row);
    $(cell).attr('col', col);
}
function populatePlayers(players, currentPlayer) {
    $('#players').html("");
    for(var playerName in players) {
        $('#players').append(buildPlayerRow(playerName, players[playerName]));
    }

    if (isActive) {
        $("tr#" + currentPlayer).addClass("current-player");
    }
}

function leave() {
    location.href = api + "/leave";
}

function buildPlayerRow(name, playerInfo) {
    var tr = $("<tr id=\"" + name + "\"></tr>");
    tr.append("<td>" + name + "</td>");
    tr.append("<td>" + playerInfo.playerType + "</td>");
    tr.append("<td>" + playerInfo.score + "</td>");
    return tr;
}

function onResize(){
    $("div.board-container").height($("div.board-container").width());
    var cellWidth = $("td.cell").width();
    $("td.cell").height(cellWidth);
}

function onMakeMove(){
    if (isMyTurn) {
        if ($('input[name=moveType]:checked').length) {
            if (selectedCells.length) {
                var moveType = $('input[name=moveType]:checked').val();
                var postRequestData = {
                    'moveType': moveType,
                    'selectedCells': selectedCells
                };

                //POST request here...
                $.ajax({
                    method: 'POST',
                    url: api + '/move',
                    data: postRequestData,
                    success: onMoveSuccess,
                    error: onMoveFailed
                });
            }
            else {
                //no selected cells error
                informUser("You haven't chosen any squares to do stuff on.", "#ff491c");
            }
        }
        else {
            //no chosen moveType error
            informUser("You haven't chosen a move type (filled/blank/undefined).", "#ff491c");
        }
    }
    else {
        informUser("Hey! It's not your turn yet :P", "#ff491c");
    }
}

function onMoveSuccess(playerInfo) {
    isMyTurn = playerInfo.name == username;
    isTurnsOver = playerInfo.isTurnsOver;
    clearSelectedCells();
    currMove = Math.min(totalMoves, playerInfo.move);
    adjustControls(playerInfo);
    if(isMyTurn) {
        populatePlayerInfo(playerInfo);
        updateBoard(playerInfo.boardInfo);
    }
}

function onNextTurnSuccess(playerInfo) {
    isMyTurn = false;
    isTurnsOver = false;
    currMove = 1;
    disableControls();
}

function populatePlayerInfo(playerInfo) {
    $("#player-score").html(playerInfo.score);
    $("#player-move").html(currMove);
}

function clearSelectedCells() {
    for(var coordKey in selectedCells) {
        var coord = selectedCells[coordKey];
        var id = coord.x + "-" + coord.y;
        $("#" + id).removeClass("selected");
    }
    selectedCells = [];
}

function adjustControls(playerInfo) {
    if (userType == 'Human') {
        if (!isMyTurn) {
            currMove = 1;
            disableControls();
        }

        if (playerInfo.isTurnsOver) {
            $("#move-button").prop("disabled", "disabled");
        }
        else if (isMyTurn){
            $("#move-button").prop("disabled", "");
        }
    }
}

function disableControls() {
    setControlsDisabledProperty("disabled");
}

function setControlsDisabledProperty(value) {
    $("#move-button").prop("disabled", value);
    $("#next-button").prop("disabled", value);
    $("#undo-button").prop("disabled", value);
    $("#redo-button").prop("disabled", value);
}

function onMoveFailed(err) {
    console.log('move error', err);
}

function informUser(message, color, time) {
    $(".controls-message")
        .html(message)
        .css('color', color)
        .css('opacity', '100');

    window.clearTimeout(hideMessageTimeout);
    hideMessageTimeout = setTimeout(hideControlsMessage, time || 4000);
}

function hideControlsMessage() {
    $(".controls-message").css('opacity', '0');
}

function onUndo() {
    sendMove("undo");
}

function onRedo() {
    sendMove("redo");
}

function onNextTurn() {
    sendMove("skip");
}

function sendMove(moveType) {
    var onSuccess = moveType == 'skip' ? onNextTurnSuccess : onMoveSuccess;
    $.ajax({
        method: 'GET',
        url: api + "/move?type=" + moveType,
        success: onSuccess,
        error: function(err) {
            console.log(moveType + ' error', err);
        }
    });
}